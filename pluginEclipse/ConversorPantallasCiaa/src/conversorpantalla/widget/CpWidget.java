package conversorpantalla.widget;


public interface CpWidget {

	public enum TypeWidget {
		CP_WIN,
		CP_LABEL,
		CP_TEXTBOX,
		CP_TERMINAL,
		CP_BUTTON,
		CP_INDICATOR,
		CP_PROGRESSBAR,
		CP_GRAPHICXY
	}
	
	public TypeWidget getTipoWidget();
	
	public String convertirContenido();
	
	public String generarLlamado();
	
	public String getNombre();
}
