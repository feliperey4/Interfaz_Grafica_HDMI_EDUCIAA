package conversorpantalla.widget.utils;

public class CpBorder {
	
	private CpColor color= new CpColor();
	private int grueso=1;
	
	public CpColor getColor() {
		return color;
	}
	public void setColor(CpColor color) {
		this.color = color;
	}
	public int getGrueso() {
		return grueso;
	}
	public void setGrueso(int grueso) {
		this.grueso = grueso;
	}	
	
	public String convertir() {
		StringBuilder str=new StringBuilder();
		str.append(".border = { \r\n");
		str.append(".gross = "+grueso +",\r\n");
		str.append(color.convertir());
		str.append("},\r\n");
		return str.toString();
	}
	
}
