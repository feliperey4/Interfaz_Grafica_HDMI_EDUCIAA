package conversorpantalla.widget.utils;

import conversorpantalla.exception.CpException;
import conversorpantalla.exception.ErrorCP;

public class CpColor {

	private int r=0;
	private int g=0;
	private int b=0;
	
	public CpColor(){
		
	}
	
	public CpColor(String col) throws CpException{
		String strArr[]=col.replace("rgb(", "").replace(")","").split(",");
		if(strArr.length!=3)throw new CpException(ErrorCP.COLOR_INV_RGB);
		r=Integer.valueOf(strArr[0].trim());
		g=Integer.valueOf(strArr[1].trim());
		b=Integer.valueOf(strArr[2].trim());
	}
	
	public int getR() {
		return r;
	}
	public void setR(int r) {
		this.r = r;
	}
	public int getG() {
		return g;
	}
	public void setG(int g) {
		this.g = g;
	}
	public int getB() {
		return b;
	}
	public void setB(int b) {
		this.b = b;
	}

	public String convertir() {
		String str=".color = DLO_RGB("+r+", "+g+", "+b+"),\r\n";
		return str;
	}
	
	
	
}
