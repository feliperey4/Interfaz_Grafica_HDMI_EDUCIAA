package conversorpantalla.widget.utils;

import conversorpantalla.exception.CpException;
import conversorpantalla.exception.ErrorCP;

public class CpFont {

	private String style="&DejaVuSansBold9ptFontInfo";
	private int tam=9;
	private CpColor color=new CpColor();
	
	public void setStyleAndTam(String estilo) throws CpException{
		String strArr[]=estilo.split("\"");
		
		if(strArr.length!=2)throw new CpException(ErrorCP.FUENTE_FMT_INV);
		style="&"+strArr[1].trim();
		strArr=strArr[0].trim().split(" ");
		int l=strArr.length;
		
		tam=Integer.valueOf(strArr[l-1].replace("pt","").trim());
		String bold="";
		String italic="";
		if(l>1){
			bold= strArr[0].trim().equals("75")?"Bold":"";
			italic= strArr[l-2].trim().equals("italic") ?"Italic":"";
		}
		
		style=style.replace(" ", "")+bold+italic+tam+"ptFontInfo";
	}
	
	public String convertir() {
		StringBuilder str=new StringBuilder();
		str.append(".font = { \r\n");
		str.append(".style = "+style +",\r\n");
		str.append(".size = "+tam +",\r\n");
		str.append(color.convertir());
		str.append("},\r\n");
		return str.toString();
	}
	
	public String getStyle() {
		return style;
	}
	public void setStyle(String style) {
		this.style = style;
	}
	public int getTam() {
		return tam;
	}
	public void setTam(int tam) {
		this.tam = tam;
	}
	public CpColor getColor() {
		return color;
	}
	public void setColor(CpColor color) {
		this.color = color;
	}
	
	
}
