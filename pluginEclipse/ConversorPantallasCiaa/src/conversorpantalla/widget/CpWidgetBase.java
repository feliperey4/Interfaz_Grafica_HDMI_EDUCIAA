package conversorpantalla.widget;

import java.util.Properties;

import org.w3c.dom.DOMException;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import conversorpantalla.exception.CpException;
import conversorpantalla.utils.XmlUtils;
import conversorpantalla.widget.utils.CpBorder;
import conversorpantalla.widget.utils.CpColor;
import conversorpantalla.widget.utils.CpFont;

public class CpWidgetBase {

	private static final String PROP_DIM = "geometry";
	private static final String PROP_TITLE = "windowTitle";
	private static final String PROP_TEXT = "text";
	private static final String PROP_STYLESHEET = "styleSheet";
	private static final String PROP_VALUE = "value";
	private static final String PROP_FLOW = "flow";
	private static final String PROP_CHECKED = "checked";

	private int posX = 0;
	private int posY = 0;
	private int ancho = 0;
	private int alto = 0;
	private CpFont fuente = new CpFont() ;
	private String texto = "";
	private CpColor background;
	private CpBorder borde ;
	private String nombreWidget = "";
	private Properties props=new Properties();

	public Properties getProps() {
		return props;
	}

	public CpWidgetBase(Element widget) throws DOMException, CpException {
		setNombreWidget(widget.getAttribute("name"));
		NodeList widgets = widget.getChildNodes();
		for (int i = 0; i < widgets.getLength(); i++) {
			Node nodo = widgets.item(i);
			if (nodo instanceof Element
					&& nodo.getNodeName().equalsIgnoreCase("property")) {
				String name = nodo.getAttributes().getNamedItem("name")
						.getTextContent();

				// 1.0 Busca propiedad de dimensiones.
				if (name.equalsIgnoreCase(PROP_DIM)) {
					cargarDim(XmlUtils.getChildByTag(nodo, "rect"));
					//System.out.println(" -> Dimensiones:  X=" + posX + "  Y="
						//	+ posY + " Ancho=" + ancho + " Alto=" + alto);
				} else if (name.equalsIgnoreCase(PROP_TITLE)
						|| name.equalsIgnoreCase(PROP_TEXT) || name.equalsIgnoreCase(PROP_VALUE)) {
					texto = nodo.getTextContent().trim();
				} else if (name.equalsIgnoreCase(PROP_STYLESHEET)) {
					leerStyleSheet(nodo.getTextContent().trim());
				}
				else if (name.equalsIgnoreCase(PROP_FLOW)) {
					props.put(PROP_FLOW, nodo.getTextContent().trim());
				}
				else if (name.equalsIgnoreCase(PROP_CHECKED)) {
					props.put(PROP_CHECKED, nodo.getTextContent().trim());
				}
			}
		}

	}

	private void leerStyleSheet(String trim) throws CpException {
		String strArr[] = trim.split(";");
		for (String str : strArr) {
			procesarStyle(str.trim());
		}
	}

	private void procesarStyle(String trim) throws CpException {
		String strArr[] = trim.split(":");
		if (strArr.length > 2)
			return;
		
		props.put(strArr[0].trim(), strArr[1].trim());
		switch (strArr[0].trim()) {
		case "color":
			fuente.setColor(new CpColor(strArr[1]));
			break;
		case "font":
			fuente.setStyleAndTam(strArr[1]);
			break;
		case "border-color":
			if(borde==null)borde=new CpBorder();
			borde.setColor(new CpColor(strArr[1]));
			break;
		case "background-color":
			background=new CpColor(strArr[1]);
			break;
		default:
			break;
		}

	}

	private void cargarDim(Node prop) {
		NodeList nodeRect = prop.getChildNodes();
		for (int i = 0; i < nodeRect.getLength(); i++) {
			Node nodo = nodeRect.item(i);
			if (nodo instanceof Element) {
				if (nodo.getNodeName().equalsIgnoreCase("x")) {
					posX = Integer.valueOf(nodo.getTextContent());
				} else if (nodo.getNodeName().equalsIgnoreCase("y")) {
					posY = Integer.valueOf(nodo.getTextContent());
				} else if (nodo.getNodeName().equalsIgnoreCase("width")) {
					ancho = Integer.valueOf(nodo.getTextContent());
				} else if (nodo.getNodeName().equalsIgnoreCase("height")) {
					alto = Integer.valueOf(nodo.getTextContent());
				}
			}
		}
	}
	
	protected String convertirDim() {
		StringBuilder str=new StringBuilder();
		str.append(".dim = { \r\n");
		str.append(".x = WIN_POS_X + "+posX +",\r\n");
		str.append(".y = WIN_POS_Y + "+posY +",\r\n");
		str.append(".height = "+alto+",\r\n");
		str.append(".width = "+ancho+",\r\n");
		str.append("},\r\n");
		return str.toString();
	}

	protected String convertirBackground(String nom) {
		StringBuilder str=new StringBuilder();
		str.append("."+nom+" = { \r\n");
		str.append(background.convertir());
		str.append("},\r\n");
		return str.toString();
	}
	
	public int getPosX() {
		return posX;
	}

	public void setPosX(int posX) {
		this.posX = posX;
	}

	public int getPosY() {
		return posY;
	}

	public void setPosY(int posY) {
		this.posY = posY;
	}

	public int getAncho() {
		return ancho;
	}

	public void setAncho(int ancho) {
		this.ancho = ancho;
	}

	public int getAlto() {
		return alto;
	}

	public void setAlto(int alto) {
		this.alto = alto;
	}

	public CpFont getFuente() {
		return fuente;
	}

	public void setFuente(CpFont fuente) {
		this.fuente = fuente;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public CpColor getBackground() {
		return background;
	}

	public void setBackground(CpColor background) {
		this.background = background;
	}

	public CpBorder getBorde() {
		return borde;
	}

	public void setBorde(CpBorder borde) {
		this.borde = borde;
	}

	public String getNombreWidget() {
		return nombreWidget;
	}

	public void setNombreWidget(String nombreWidget) {
		this.nombreWidget = nombreWidget;
	}

}
