package conversorpantalla.widget.impl;

import org.w3c.dom.DOMException;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import conversorpantalla.exception.CpException;
import conversorpantalla.widget.CpWidget;
import conversorpantalla.widget.CpWidgetBase;

public class CpLabel extends CpWidgetBase implements CpWidget{

	public CpLabel(Node nodo) throws DOMException, CpException {
		super((Element)nodo);
	}

	@Override
	public String convertirContenido() {
		StringBuilder str= new StringBuilder();
		str.append("static paintLabel_t "+getNombreWidget()+"_desc = {\r\n");
		str.append(convertirDim());
		str.append(getFuente().convertir());
		str.append(".text = \""+getTexto()+"\",\r\n");
		str.append("};\r\n");		
		return str.toString();
	}

	@Override
	public TypeWidget getTipoWidget() {
		return TypeWidget.CP_LABEL;
	}

	@Override
	public String generarLlamado() {
		StringBuilder str= new StringBuilder();
		str.append("{\r\n");
		str.append(".type =	OBJ_LABEL,\r\n");
		str.append(".obj = (paintGenericElement_t) &"+getNombreWidget()+"_desc,\r\n");
		str.append("}");
		return str.toString();
	}
	
	@Override
	public String getNombre() {
		return getNombreWidget();
	}

}
