package conversorpantalla.widget.impl;

import org.w3c.dom.DOMException;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import conversorpantalla.exception.CpException;
import conversorpantalla.widget.CpWidget;
import conversorpantalla.widget.CpWidgetBase;
import conversorpantalla.widget.utils.CpBorder;
import conversorpantalla.widget.utils.CpColor;

public class CpTerminal extends CpWidgetBase implements CpWidget{

	private int flow=0;
	
	public CpTerminal(Node nodo) throws DOMException, CpException {
		super((Element)nodo);
		
		//1.0 Verifica los valores por defectos.
		if(getBackground()==null){
			setBackground(new CpColor("rgb(20, 20,20)"));
		}
		
		if(getBorde()==null){
			CpBorder borde= new CpBorder();
			borde.setColor(new CpColor("rgb(105, 105, 105)"));
			borde.setGrueso(3);
			setBorde(borde);			
		}
		
		if(getProps().containsKey("flow")){
			flow=Integer.valueOf(getProps().getProperty("flow"));
		}
	}

	@Override
	public String convertirContenido() {
		StringBuilder str= new StringBuilder();
		str.append("static paintTerminal_t "+getNombreWidget()+"_desc = {\r\n");
		str.append(convertirDim());
		str.append(convertirBackground("backGround"));
		str.append(getBorde().convertir());
		str.append(getFuente().convertir());
		str.append(".flow = "+(flow==0?"TERMINAL_FLOW_UP":"TERMINAL_FLOW_FALLING")+",\r\n");
		str.append(".text = \""+getTexto()+"\",\r\n");
		str.append("};\r\n");		
		return str.toString();
	}

	@Override
	public TypeWidget getTipoWidget() {
		return TypeWidget.CP_TERMINAL;
	}

	@Override
	public String generarLlamado() {
		StringBuilder str= new StringBuilder();
		str.append("{\r\n");
		str.append(".type =	OBJ_TERMINAL,\r\n");
		str.append(".obj = (paintGenericElement_t) &"+getNombreWidget()+"_desc,\r\n");
		str.append("}");
		return str.toString();
	}
	
	@Override
	public String getNombre() {
		return getNombreWidget();
	}

}
