package conversorpantalla.widget.impl;

import org.w3c.dom.DOMException;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import conversorpantalla.exception.CpException;
import conversorpantalla.widget.CpWidget;
import conversorpantalla.widget.CpWidgetBase;
import conversorpantalla.widget.utils.CpBorder;
import conversorpantalla.widget.utils.CpColor;

public class CpButton extends CpWidgetBase implements CpWidget{

	private CpColor backGroundPress = new CpColor("rgb(3, 133, 189)");
	
	public CpButton(Node wg) throws DOMException, CpException {
		super((Element)wg);
		
		//1.0 Verifica los valores por defectos.
		if(getBackground()==null){
			setBackground(new CpColor("rgb(210, 210, 210)"));
		}
		
		if(getBorde()==null){
			CpBorder borde= new CpBorder();
			borde.setColor(new CpColor("rgb(105, 105, 105)"));
			borde.setGrueso(2);
			setBorde(borde);			
		}	
		
		if(getProps().containsKey("selection-background-color"))backGroundPress=new CpColor(getProps().getProperty("selection-background-color"));
	}
	
	private String convertirBackgroundPress() {
		StringBuilder str=new StringBuilder();
		str.append(".backGroundPress = { \r\n");
		str.append(backGroundPress.convertir());
		str.append("},\r\n");
		return str.toString();
	}

	@Override
	public String convertirContenido() {
		StringBuilder str= new StringBuilder();
		str.append("static paintButton_t "+getNombreWidget()+"_desc = {\r\n");
		str.append(convertirDim());
		str.append(convertirBackground("backGroundReleased"));
		str.append(convertirBackgroundPress());
		str.append(getBorde().convertir());
		str.append(getFuente().convertir());
		str.append(".text = \""+getTexto()+"\",\r\n");
		str.append("};\r\n");		
		return str.toString();
	}

	@Override
	public TypeWidget getTipoWidget() {
		return TypeWidget.CP_BUTTON;
	}

	@Override
	public String generarLlamado() {
		StringBuilder str= new StringBuilder();
		str.append("{\r\n");
		str.append(".type =	OBJ_BUTTON,\r\n");
		str.append(".obj = (paintGenericElement_t) &"+getNombreWidget()+"_desc,\r\n");
		str.append("}");
		return str.toString();
	}
	
	@Override
	public String getNombre() {
		return getNombreWidget();
	}

}
