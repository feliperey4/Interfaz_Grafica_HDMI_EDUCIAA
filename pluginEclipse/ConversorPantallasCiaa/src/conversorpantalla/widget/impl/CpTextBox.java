package conversorpantalla.widget.impl;

import org.w3c.dom.DOMException;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import conversorpantalla.exception.CpException;
import conversorpantalla.widget.CpWidget;
import conversorpantalla.widget.CpWidgetBase;
import conversorpantalla.widget.utils.CpBorder;
import conversorpantalla.widget.utils.CpColor;

public class CpTextBox extends CpWidgetBase implements CpWidget{

	public CpTextBox(Node nodo) throws DOMException, CpException {
		super((Element)nodo);
		
		//1.0 Verifica los valores por defectos.
		if(getBackground()==null){
			System.out.println("Es nulo");
			setBackground(new CpColor("rgb(246,246, 246)"));
		}
		
		if(getBorde()==null){
			CpBorder borde= new CpBorder();
			borde.setColor(new CpColor("rgb(190, 190, 190)"));
			borde.setGrueso(1);
			setBorde(borde);			
		}
	}

	@Override
	public String convertirContenido() {
		StringBuilder str= new StringBuilder();
		str.append("static paintTextBox_t "+getNombreWidget()+"_desc = {\r\n");
		str.append(convertirDim());
		str.append(convertirBackground("backGround"));
		str.append(getBorde().convertir());
		str.append(getFuente().convertir());
		str.append(".text = \""+getTexto()+"\",\r\n");
		str.append("};\r\n");		
		return str.toString();
	}

	@Override
	public TypeWidget getTipoWidget() {
		return TypeWidget.CP_TEXTBOX;
	}

	@Override
	public String generarLlamado() {
		StringBuilder str= new StringBuilder();
		str.append("{\r\n");
		str.append(".type =	OBJ_TEXT_BOX,\r\n");
		str.append(".obj = (paintGenericElement_t) &"+getNombreWidget()+"_desc,\r\n");
		str.append("}");
		return str.toString();
	}
	
	@Override
	public String getNombre() {
		return getNombreWidget();
	}

}
