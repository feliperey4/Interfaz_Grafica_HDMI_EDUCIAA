package conversorpantalla.widget.impl;

import org.w3c.dom.DOMException;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import conversorpantalla.exception.CpException;
import conversorpantalla.widget.CpWidget;
import conversorpantalla.widget.CpWidgetBase;
import conversorpantalla.widget.utils.CpBorder;
import conversorpantalla.widget.utils.CpColor;

public class CpIndicator extends CpWidgetBase implements CpWidget{

	private CpColor backGroundOn = new CpColor("rgb(0, 255, 0)");
	private boolean checked=false;
	
	public CpIndicator(Node wg) throws DOMException, CpException {
		super((Element)wg);
		
		//1.0 Verifica los valores por defectos.
		if(getBackground()==null){
			setBackground(new CpColor("rgb(255, 0, 0)"));
		}
		
		if(getBorde()==null){
			CpBorder borde= new CpBorder();
			borde.setColor(new CpColor("rgb(20, 20, 20)"));
			borde.setGrueso(4);
			setBorde(borde);			
		}	
		
		if(getProps().containsKey("selection-background-color"))backGroundOn=new CpColor(getProps().getProperty("selection-background-color"));
		if(getProps().containsKey("checked"))checked=Boolean.valueOf(getProps().getProperty("checked"));
	}
	
	private String convertirBackGroundOn() {
		StringBuilder str=new StringBuilder();
		str.append(".backGroundOn = { \r\n");
		str.append(backGroundOn.convertir());
		str.append("},\r\n");
		return str.toString();
	}

	@Override
	public String convertirContenido() {
		StringBuilder str= new StringBuilder();
		str.append("static paintIndicator_t "+getNombreWidget()+"_desc = {\r\n");
		str.append(convertirDim());
		str.append(convertirBackground("backGroundOff"));
		str.append(convertirBackGroundOn());
		str.append(getBorde().convertir());
		str.append(".state = "+(checked?"INDICATOR_ON":"INDICATOR_OFF")+",\r\n");
		str.append("};\r\n");		
		return str.toString();
	}

	@Override
	public TypeWidget getTipoWidget() {
		return TypeWidget.CP_INDICATOR;
	}

	@Override
	public String generarLlamado() {
		StringBuilder str= new StringBuilder();
		str.append("{\r\n");
		str.append(".type =	OBJ_INDICATOR,\r\n");
		str.append(".obj = (paintGenericElement_t) &"+getNombreWidget()+"_desc,\r\n");
		str.append("}");
		return str.toString();
	}
	
	@Override
	public String getNombre() {
		return getNombreWidget();
	}

}
