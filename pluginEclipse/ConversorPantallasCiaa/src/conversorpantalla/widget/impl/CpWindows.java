package conversorpantalla.widget.impl;

import java.util.ArrayList;
import java.util.List;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import conversorpantalla.exception.CpException;
import conversorpantalla.widget.CpWidget;
import conversorpantalla.widget.CpWidgetBase;
import conversorpantalla.widget.utils.CpBorder;
import conversorpantalla.widget.utils.CpColor;

public class CpWindows extends CpWidgetBase implements CpWidget{

	private List<CpWidget> listaWidgets= new ArrayList<CpWidget>();
	
	
	public List<CpWidget> getListaWidgets() {
		return listaWidgets;
	}

	public CpWindows(Element widget) throws CpException {
		super(widget);
		
		//1.0 Verifica los valores por defectos.
		if(getBackground()==null){
			setBackground(new CpColor("rgb(227, 227, 227)"));
		}
		
		if(getBorde()==null){
			CpBorder borde= new CpBorder();
			borde.setColor(new CpColor("rgb(51, 153, 255)"));
			borde.setGrueso(10);
			setBorde(borde);			
		}
		
		generarListawidgets(widget);
		getBorde().setGrueso(getFuente().getTam()+1);
	}

	private void generarListawidgets(Element widget) throws CpException {
		XPath xpath = XPathFactory.newInstance().newXPath();
		try {
			NodeList comps = (NodeList) xpath.evaluate("//widget[@class='QWidget']", widget, XPathConstants.NODESET);
			comps=comps.item(0).getChildNodes();
			
			for(int i=0;i<comps.getLength();i++){
				Node nodo = comps.item(i);
				if (nodo instanceof Element) {
					String name = nodo.getAttributes().getNamedItem("name")
							.getTextContent();
					
					if(name.startsWith(TypeWidget.CP_LABEL.name())){
						listaWidgets.add(new CpLabel(nodo));
					}
					else if(name.startsWith(TypeWidget.CP_TEXTBOX.name())){
						listaWidgets.add(new CpTextBox(nodo));
					}
					else if(name.startsWith(TypeWidget.CP_BUTTON.name())){
						listaWidgets.add(new CpButton(nodo));
					}
					else if(name.startsWith(TypeWidget.CP_PROGRESSBAR.name())){
						listaWidgets.add(new CpProgressBar(nodo));
					}
					else if(name.startsWith(TypeWidget.CP_TERMINAL.name())){
						listaWidgets.add(new CpTerminal(nodo));
					}
					else if(name.startsWith(TypeWidget.CP_INDICATOR.name())){
						listaWidgets.add(new CpIndicator(nodo));
					}
					
				}
			}
			
		} catch (XPathExpressionException e) {
			throw new CpException(e);
		}
	}

	@Override
	public String convertirContenido() {
		StringBuilder str= new StringBuilder();
		str.append("static paintWindows_t "+getNombreWidget()+"_desc = {\r\n");
		str.append(convertirDim());
		str.append(convertirBackground("backGround"));
		str.append(getBorde().convertir());
		str.append(getFuente().convertir());
		str.append(".text = \""+getTexto()+"\",\r\n");
		str.append("};\r\n");		
		str.append("\r\n");
		for(CpWidget wg:listaWidgets){
			str.append(wg.convertirContenido());
			str.append("\r\n");
		}
		return str.toString();
	}

	@Override
	public TypeWidget getTipoWidget() {
		return TypeWidget.CP_WIN;
	}

	@Override
	public String generarLlamado() {
		StringBuilder str= new StringBuilder();
		str.append("static paintElement_t "+getNombreWidget()+" = {\r\n");
		str.append(".type =	OBJ_WINDOWS,\r\n");
		str.append(".obj = (paintGenericElement_t) &"+getNombreWidget()+"_desc,\r\n");
		str.append("};\r\n");
		return str.toString();
	}
	
	public String generarListaComponentes(){
		StringBuilder str= new StringBuilder();
		boolean flag=true;
		str.append("typedef enum {\r\n");
		for(CpWidget wg:listaWidgets){
			str.append((flag?"":",\r\n")+wg.getNombre());
			flag=false;
		}		
		str.append("\r\n} listComponent_"+getNombreWidget()+";\r\n");
		return str.toString();
	}

	@Override
	public String getNombre() {
		return getNombreWidget();
	}

	public String generarArregloComp() {
		StringBuilder str= new StringBuilder();
		boolean flag=true;
		str.append("extern paintElement_t winComponent_"+getNombreWidget()+"[WIN_NUM_COMPONENT] = {\r\n");
		for(CpWidget wg:listaWidgets){
			str.append((flag?"":",\r\n")+wg.generarLlamado());
			flag=false;
		}		
		str.append("\r\n};\r\n");
		return str.toString();
	}
	
	public String generarObjVentana() {
		StringBuilder str= new StringBuilder();
		str.append("extern paintPanel_t panelWin_"+getNombreWidget()+" = {\r\n");
		str.append(".panel = &"+getNombreWidget()+",\r\n");
		str.append(".numComp = WIN_NUM_COMPONENT,\r\n");
		str.append(".elements = winComponent_"+getNombreWidget()+",\r\n");
		str.append("};\r\n");
		return str.toString();
	}
	
	

}
