package conversorpantalla.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import conversorpantalla.exception.CpException;
import conversorpantalla.widget.impl.CpWindows;

public class Test {

	public static void main(String[] args) throws FileNotFoundException, SAXException, IOException, ParserConfigurationException, CpException {
		// TODO Auto-generated method stub

		String rutaArchivo="C:\\Qt\\Test_2\\test\\mainwindow.ui";
		String rutaSal="C:\\Desarrollo\\felipe\\test_pantallas\\MiVentana.h";
		
		
		DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();  
		Document doc = documentBuilder.parse(new FileInputStream(rutaArchivo)); 
		
		Element elementRaiz = doc.getDocumentElement();  
		
		Element elemtVent=CpUtils.buscarVentana(elementRaiz);
		//System.out.println("");
		
		CpWindows win= new CpWindows(elemtVent);
		
		//System.out.println(CpUtils.generarH(win));
		
		CpUtils.guardarArchivo(rutaSal, CpUtils.generarH(win));
		
		System.out.println("-> Termino test.");  
	}

}
