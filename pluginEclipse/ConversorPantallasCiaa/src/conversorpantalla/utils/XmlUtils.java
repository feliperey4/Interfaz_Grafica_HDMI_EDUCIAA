package conversorpantalla.utils;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class XmlUtils {

	
	public static Node getChildByTag(Node nodo,String tag){
		Node sal=null;
		NodeList nodeRect =  nodo.getChildNodes();
		for (int i = 0; i < nodeRect.getLength(); i++) {
			Node aux = nodeRect.item(i);
			if (aux instanceof Element && aux.getNodeName().trim().equalsIgnoreCase(tag)) {
				sal=aux;
				break;
			}
		}
		return sal;
	}
	
}
