package conversorpantalla.utils;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import conversorpantalla.exception.CpException;
import conversorpantalla.exception.ErrorCP;
import conversorpantalla.widget.impl.CpWindows;

public class CpUtils {
	
	private static final String WIDGET_VENT="CP_WIN";
	private static final String ENTER="\r\n";

	public static Element buscarVentana(Element doc) throws CpException {
		Element ventana = null;
		NodeList widgets = doc.getElementsByTagName("widget");
		for (int i = 0; i < widgets.getLength(); i++) {
			Node nodo = widgets.item(i);
			if (nodo instanceof Element) {
				String name = nodo.getAttributes().getNamedItem("name")
						.getTextContent();
				
				if(name.startsWith(WIDGET_VENT)){
					//System.out.println("---> Ventana: " + name);
					ventana=(Element) nodo;
					break;
				}
				
			}
		}
		
		if(ventana==null) throw new CpException(ErrorCP.BUSCAR_VENT);

		return ventana;
	}

	public static String generarH(CpWindows win) {
		StringBuilder str= new StringBuilder();
		str.append("/*==================[inclusions]=============================================*/"+ENTER);
		str.append("#include \"USB.h\""+ENTER);
		str.append(ENTER);
		str.append("/*==================[macros]=================================================*/"+ENTER);
		str.append("#define WIN_NUM_COMPONENT "+win.getListaWidgets().size()+ENTER);
		str.append("#define WIN_POS_X "+win.getPosX()+ENTER);
		str.append("#define WIN_POS_Y "+win.getPosY()+ENTER);
		str.append(ENTER);
		str.append("/*==================[typedef]=================================================*/"+ENTER);
		str.append(win.generarListaComponentes());
		str.append(ENTER);
		str.append("/*==================[external data declaration]==============================*/"+ENTER);
		str.append(ENTER);
		str.append(win.convertirContenido());
		str.append(win.generarLlamado());
		str.append(ENTER);
		str.append(win.generarArregloComp());
		str.append(ENTER);
		str.append(win.generarObjVentana());
		str.append(ENTER);
		str.append("/*==================[external functions declaration]=========================*/"+ENTER);
		str.append(ENTER);
		str.append("/*==================[end of file]============================================*/"+ENTER);
		//System.out.println(str.toString());
		return str.toString();
	}
	
	public static void guardarArchivo(String ruta,String contenido){
		PrintWriter pw;
		try {
			pw = new PrintWriter( ruta );
			pw.print(contenido);
			pw.flush();
			pw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
}
