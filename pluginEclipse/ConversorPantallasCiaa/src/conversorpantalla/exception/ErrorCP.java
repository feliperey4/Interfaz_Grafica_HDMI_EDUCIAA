package conversorpantalla.exception;

public enum ErrorCP {

	BUSCAR_VENT(1,"No se encuentra el widget de ventana"),
	COLOR_INV_RGB(2,"Color invalido. No se puede transformar el color."),
	FUENTE_FMT_INV(3,"Formato de fuente invalida. No se puede transformar la fuente.");
	
	
	private int code;
	private String msg;
	
	private ErrorCP(int code,String msg){
		this.code=code;
		this.msg=msg;
	}

	public int getCode() {
		return code;
	}

	public String getMsg() {
		return msg;
	}
	
}
