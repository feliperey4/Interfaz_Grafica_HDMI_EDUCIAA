package conversorpantalla.exception;



public class CpException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private ErrorCP cpError;

	public CpException() {
		super();
	}

	public CpException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public CpException(String message, Throwable cause) {
		super(message, cause);
	}

	public CpException(String message) {
		super(message);
	}

	public CpException(Throwable cause) {
		super(cause);
	}

	public CpException(ErrorCP err) {
		super(" code: "+err.getCode()+ " - "+err.getMsg());
		cpError=err;
	}
	
	public ErrorCP getCpError(){
		return cpError;
	}
}
