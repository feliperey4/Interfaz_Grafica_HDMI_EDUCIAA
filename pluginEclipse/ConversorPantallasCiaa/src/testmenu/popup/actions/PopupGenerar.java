package testmenu.popup.actions;

import java.io.File;

import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.dialogs.ContainerSelectionDialog;

public class PopupGenerar extends Shell {
	private Text text;
	private Text text_1;

	private static final String[] FILTER_NAMES = {
	      "Qt UI File (*.ui)"};

	  // These filter extensions are used to filter which files are displayed.
	  private static final String[] FILTER_EXTS = { "*.ui"};
	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String args[]) {
		try {
			Display display = Display.getDefault();
			PopupGenerar shell = new PopupGenerar(display);
			shell.open();
			shell.layout();
			while (!shell.isDisposed()) {
				if (!display.readAndDispatch()) {
					display.sleep();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the shell.
	 * @param display
	 */
	public PopupGenerar(Display display) {
		super(display, SWT.SHELL_TRIM);
		setText("Importar pantallas");
		setSize(577, 334);
		
		Button btnNewButton = new Button(this, SWT.NONE);
		btnNewButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				handleBrowsePantalla();
			}
		});
		btnNewButton.setBounds(458, 42, 75, 25);
		btnNewButton.setText("Browse...");
		
		text = new Text(this, SWT.BORDER);
		text.setBounds(10, 44, 435, 21);
		
		Label lblNewLabel = new Label(this, SWT.NONE);
		lblNewLabel.setBounds(10, 23, 150, 15);
		lblNewLabel.setText("Archivo de la pantalla:");
		
		Button btnNewButton_1 = new Button(this, SWT.NONE);
		btnNewButton_1.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				salir();
			}
		});
		btnNewButton_1.setBounds(445, 261, 75, 25);
		btnNewButton_1.setText("Cancelar");
		
		Button btnGenerar = new Button(this, SWT.NONE);
		btnGenerar.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(validarContenido()){
					salir();
				}
			}
			
		});
		btnGenerar.setBounds(348, 261, 75, 25);
		btnGenerar.setText("Generar");
		
		text_1 = new Text(this, SWT.BORDER);
		text_1.setBounds(10, 127, 435, 21);
		
		Button btnBrowser = new Button(this, SWT.NONE);
		btnBrowser.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				handleBrowseDirSalida();
			}
		});
		btnBrowser.setBounds(458, 123, 75, 25);
		btnBrowser.setText("Browser...");
		
		Label lblRutaDeGeneracion = new Label(this, SWT.NONE);
		lblRutaDeGeneracion.setBounds(10, 103, 122, 15);
		lblRutaDeGeneracion.setText("Ruta de generaci\u00F3n:");
		createContents();
	}

	/**
	 * Create contents of the shell.
	 */
	protected void createContents() {

	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
	
	private void handleBrowsePantalla() {
        FileDialog dlg = new FileDialog(this, SWT.MULTI);
        dlg.setFilterNames(FILTER_NAMES);
        dlg.setFilterExtensions(FILTER_EXTS);
        String fn = dlg.open();
        if (fn != null) {
          StringBuffer buf = new StringBuffer();
          String[] files = dlg.getFileNames();
          for (int i = 0, n = files.length; i < n; i++) {
            buf.append(dlg.getFilterPath());
            if (buf.charAt(buf.length() - 1) != File.separatorChar) {
              buf.append(File.separatorChar);
            }
            buf.append(files[i]);
            buf.append(" ");
          }
          text.setText(buf.toString());
        }
	}
	
	private void salir(){
		this.setVisible(false);;
	}
	
	private void handleBrowseDirSalida() {
		ContainerSelectionDialog dialog = new ContainerSelectionDialog(
				getShell(), ResourcesPlugin.getWorkspace().getRoot(), false,
				"Seleccione directorio de salida");
		if (dialog.open() == ContainerSelectionDialog.OK) {
			Object[] result = dialog.getResult();
			if (result.length == 1) {
				File file= new File(ResourcesPlugin.getWorkspace().getRoot().getLocation().toString());
				file=new File(file.getAbsoluteFile()+((Path) result[0]).toString());
				text_1.setText(file.getAbsolutePath());
			}
		}
	}
	
	private boolean validarContenido() {
		
		//1.0 valida ruta del archivo UI
		String str=text.getText().trim();
		if(str==null || str.isEmpty()){
			 MessageDialog.openError(
					 this,
					 "Importar pantallas",
					 "La ruta del archivo no puede ser vacia.");
			return false;
		}
		
		File file = new File(str);
		if(!file.exists() || !file.isFile()){
			MessageDialog.openError(
					 this,
					 "Importar pantallas",
					 "Error validando la ruta del archivo '.ui'.");
			return false;
		}
		
		//2.0 Valida directorio de salida.
		str=text_1.getText().trim();
		if(str==null || str.isEmpty()){
			MessageDialog.openError(
					 this,
					 "Importar pantallas",
					 "El directorio de salida no puede estar vacia.");
			return false;
		}
		
		return true;
	}
	
	public String getRutaArchivo(){
		return text.getText().trim();
	}
	
	public String getDirSalida(){
		return text_1.getText().trim();
	}
}
