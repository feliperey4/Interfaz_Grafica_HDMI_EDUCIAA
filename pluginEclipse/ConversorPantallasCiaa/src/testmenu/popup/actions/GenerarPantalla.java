package testmenu.popup.actions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import conversorpantalla.exception.CpException;
import conversorpantalla.utils.CpUtils;
import conversorpantalla.widget.impl.CpWindows;

public class GenerarPantalla implements IObjectActionDelegate {

	private Shell shell;
	private PopupGenerar miShell=null;

	/**
	 * Constructor for Action1.
	 */
	public GenerarPantalla() {
		super();
	}

	/**
	 * @see IObjectActionDelegate#setActivePart(IAction, IWorkbenchPart)
	 */
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		shell = targetPart.getSite().getShell();
	}

	/**
	 * @see IActionDelegate#run(IAction)
	 */
	public void run(IAction action) {
		
		Display display=shell.getDisplay();
		miShell= new PopupGenerar(display);
		
		miShell.open();
		miShell.layout();
		while (miShell.isVisible()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		System.out.println("-> Ruta de archivo: "+miShell.getRutaArchivo());
		System.out.println("-> Directorio de salida: "+miShell.getDirSalida());
		
		try {
			convertirPantalla(miShell.getRutaArchivo(),miShell.getDirSalida());
		} catch (ParserConfigurationException | SAXException | IOException
				| CpException e) {
			MessageBox dialog = new MessageBox(shell, SWT.ICON_ERROR | SWT.OK);
			dialog.setText("Error Importando");
			dialog.setMessage(e.getMessage());

			dialog.open();
			e.printStackTrace();
		}
		miShell.dispose();

		
	}

	private void convertirPantalla(String rutaArchivo, String dirSalida) throws ParserConfigurationException, FileNotFoundException, SAXException, IOException, CpException {
		System.out.println("-> Generando Archivo...");
		DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();  
		Document doc = documentBuilder.parse(new FileInputStream(rutaArchivo)); 
		Element elementRaiz = doc.getDocumentElement();  		
		Element elemtVent=CpUtils.buscarVentana(elementRaiz);
		CpWindows win= new CpWindows(elemtVent);
		
		Path path= new File(dirSalida).toPath();
		path=path.resolve(win.getTexto()+".h");
		System.out.println("-> Archivo Generado: '"+path.toFile().getAbsolutePath()+"'");
		CpUtils.guardarArchivo(path.toFile().getAbsolutePath(), CpUtils.generarH(win));
		System.out.println("-> Archivo generado correctamente.");
	}

	/**
	 * @see IActionDelegate#selectionChanged(IAction, ISelection)
	 */
	public void selectionChanged(IAction action, ISelection selection) {
	}

	

}
