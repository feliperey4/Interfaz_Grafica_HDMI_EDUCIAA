/*
 * @brief Mass Storage Host example
 *
 * @note
 * Copyright(C) NXP Semiconductors, 2012
 * Copyright(C) Dean Camera, 2011, 2012
 * All rights reserved.
 *
 * @par
 * Software that is described herein is for illustrative purposes only
 * which provides customers with programming information regarding the
 * LPC products.  This software is supplied "AS IS" without any warranties of
 * any kind, and NXP Semiconductors and its licensor disclaim any and
 * all warranties, express or implied, including all implied warranties of
 * merchantability, fitness for a particular purpose and non-infringement of
 * intellectual property rights.  NXP Semiconductors assumes no responsibility
 * or liability for the use of the software, conveys no license or rights under any
 * patent, copyright, mask work right, or any other intellectual property rights in
 * or to any products. NXP Semiconductors reserves the right to make changes
 * in the software without notification. NXP Semiconductors also makes no
 * representation or warranty that such application will be suitable for the
 * specified use without further testing or modification.
 *
 * @par
 * Permission to use, copy, modify, and distribute this software and its
 * documentation is hereby granted, under NXP Semiconductors' and its
 * licensor's relevant copyrights in the software, without fee, provided that it
 * is used in conjunction with NXP Semiconductors microcontrollers.  This
 * copyright, permission, and disclaimer notice must appear in all copies of
 * this code.
 */

#include <DisplayLinkHost.h>
#include "fsusb_cfg.h"
#include "ff.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
#define MID_REC_DIV 5
/** LPCUSBlib Mass Storage Class driver interface configuration and state information. This structure is
 *  passed to all Mass Storage Class driver functions, so that multiple instances of the same class
 *  within a device can be differentiated from one another.
 */
static USB_ClassInfo_MS_Host_t FlashDisk_MS_Interface = { .Config = {
		.DataINPipeNumber = 1, .DataINPipeDoubleBank = false,

		.DataOUTPipeNumber = 2, .DataOUTPipeDoubleBank = false,

		.DataOUT2PipeNumber = 3, .DataOUT2PipeDoubleBank = false, .PortNumber =
				0, }, };

static char buffData[BUF_TAM];

static dlo_device_t dev;

static SCSI_Capacity_t DiskCapacity;
static uint8_t buffer[8 * 1024];
static void delay();

STATIC FATFS fatFS; /* File system object */
STATIC FIL fileObj; /* File object */
/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/
static void delay() {
	uint32_t i = 0;
	uint32_t j = 0;
	uint32_t tam = 4000;
	for (i = 0; i < tam; i++) {
		for (j = 0; j < tam; j++)
			;
	}
}
void moverBall(dlo_dot_t *posIni, uint16_t vel) {
	dlo_draw_circle(&FlashDisk_MS_Interface, posIni, 50,3, DLO_RGB(0, 0, 0));

	posIni->x += vel;
	posIni->y += vel;
	dlo_draw_circle(&FlashDisk_MS_Interface, posIni, 50,3,
			DLO_RGB(250, 250, 250));

}
void testBall() {
	dlo_fill_rect(&FlashDisk_MS_Interface, NULL, NULL, DLO_RGB(0, 0, 0));
	dlo_dot_t posIni;

	posIni.x = 50;
	posIni.y = 50;

	while (1) {
		moverBall(&posIni, 5);
		delay();
	}

}
static testString() {
	dlo_dot_t pos;
	dlo_fill_rect(&FlashDisk_MS_Interface, NULL, NULL, DLO_RGB(0, 0, 0));

	pos.x = 5;
	pos.y = 30;
	dlo_draw_str(&FlashDisk_MS_Interface, &pos, DLO_RGB(255, 255, 255),
			&dejaVuSansBold9ptFontInfo, "1234567890+-*/");

	pos.x = 502;
	pos.y = 900;
	dlo_draw_str(&FlashDisk_MS_Interface, &pos, DLO_RGB(255, 25, 255),
			&dejaVuSansBold9ptFontInfo, "HOLAAAAAA!!!");

	pos.x = 800;
	pos.y = 300;
	dlo_draw_str(&FlashDisk_MS_Interface, &pos, DLO_RGB(25, 255, 255),
			&dejaVuSansBold9ptFontInfo, "Funcionaaaaa...");

	pos.x = 200;
	pos.y = 70;
	dlo_draw_str(&FlashDisk_MS_Interface, &pos, DLO_RGB(255, 255, 25),
			&dejaVuSansBold9ptFontInfo, "TE AMOOOOO!!!");
}

static testLine() {
	dlo_dot_t posIni;
	dlo_dot_t posFin;

	dlo_fill_rect(&FlashDisk_MS_Interface, NULL, NULL, DLO_RGB(0, 0, 0));

	posIni.x = 100;
	posIni.y = 100;

	posFin.x = 110;
	posFin.y = 900;
	dlo_draw_line(&FlashDisk_MS_Interface, &posIni, &posFin,
			DLO_RGB(255, 255, 25), 4);

	posIni.x = 200;
	posIni.y = 200;

	posFin.x = 900;
	posFin.y = 210;
	dlo_draw_line(&FlashDisk_MS_Interface, &posIni, &posFin,
			DLO_RGB(255, 255, 255), 7);

	posIni.x = 300;
	posIni.y = 400;

	posFin.x = 1050;
	posFin.y = 1000;
	dlo_draw_line(&FlashDisk_MS_Interface, &posIni, &posFin,
			DLO_RGB(25, 255, 255), 5);

	posIni.x = 1000;
	posIni.y = 800;

	posFin.x = 1550;
	posFin.y = 70;
	dlo_draw_line(&FlashDisk_MS_Interface, &posIni, &posFin,
			DLO_RGB(255, 25, 255), 3);

	posIni.x = 300;
	posIni.y = 800;
	dlo_draw_circle_fill(&FlashDisk_MS_Interface, &posIni, 20,
			DLO_RGB(125, 125, 155));

	posIni.x = 1500;
	posIni.y = 800;
	dlo_draw_circle(&FlashDisk_MS_Interface, &posIni, 200, 20,
			DLO_RGB(25, 25, 255));

	posIni.x = 1300;
	posIni.y = 200;
	dlo_draw_circle_fill(&FlashDisk_MS_Interface, &posIni, 43,
			DLO_RGB(125, 2, 155));

	posIni.x = 1000;
	posIni.y = 600;
	dlo_draw_circle_fill(&FlashDisk_MS_Interface, &posIni, 30,
			DLO_RGB(225, 125, 115));
}

static testDL() {
	uint32_t i;
	dlo_rect_t rec;
	dlo_view_t *view;

	dlo_fill_rect(&FlashDisk_MS_Interface, NULL, NULL, DLO_RGB(0, 0, 0));
	delay();

	dlo_fill_rect(&FlashDisk_MS_Interface, NULL, NULL, DLO_RGB(54, 0, 230));
	delay();

	dlo_fill_rect(&FlashDisk_MS_Interface, NULL, NULL, DLO_RGB(0, 230, 54));
	delay();

	dlo_fill_rect(&FlashDisk_MS_Interface, NULL, NULL, DLO_RGB(230, 54, 0));
	delay();

	dlo_fill_rect(&FlashDisk_MS_Interface, NULL, NULL, DLO_RGB(255, 255, 255));
	delay();

	//Test de un rectangulo
	view = &dev.mode.view;
	rec.width = 384;
	rec.height = view->height;
	rec.origin.x = 0;
	rec.origin.y = 0;
	dlo_fill_rect(&FlashDisk_MS_Interface, view, &rec, DLO_RGB(252, 36, 52));

	rec.origin.x = rec.width;
	rec.width = rec.width + 384;
	dlo_fill_rect(&FlashDisk_MS_Interface, view, &rec, DLO_RGB(221, 21, 187));

	rec.origin.x = rec.width;
	rec.width = rec.width + 384;
	dlo_fill_rect(&FlashDisk_MS_Interface, view, &rec, DLO_RGB(21, 198, 120));

	rec.origin.x = rec.width;
	rec.width = rec.width + 384;
	dlo_fill_rect(&FlashDisk_MS_Interface, view, &rec, DLO_RGB(23, 70, 149));

	rec.origin.x = rec.width;
	rec.width = rec.width + 384;
	dlo_fill_rect(&FlashDisk_MS_Interface, view, &rec, DLO_RGB(30, 234, 74));

	delay();
	dlo_fill_rect(&FlashDisk_MS_Interface, NULL, NULL, DLO_RGB(255, 255, 255));

	rec.width = view->width;
	rec.height = 216;
	rec.origin.x = 0;
	rec.origin.y = 0;
	dlo_fill_rect(&FlashDisk_MS_Interface, view, &rec, DLO_RGB(252, 36, 52));

	rec.origin.y = rec.height;
	rec.height = rec.height + 216;
	dlo_fill_rect(&FlashDisk_MS_Interface, view, &rec, DLO_RGB(221, 21, 187));

	rec.origin.y = rec.height;
	rec.height = rec.height + 216;
	dlo_fill_rect(&FlashDisk_MS_Interface, view, &rec, DLO_RGB(21, 198, 120));

	rec.origin.y = rec.height;
	rec.height = rec.height + 216;
	dlo_fill_rect(&FlashDisk_MS_Interface, view, &rec, DLO_RGB(23, 70, 149));

	rec.origin.y = rec.height;
	rec.height = rec.height + 216;
	dlo_fill_rect(&FlashDisk_MS_Interface, view, &rec, DLO_RGB(30, 234, 74));

	delay();
	dlo_fill_rect(&FlashDisk_MS_Interface, NULL, NULL, DLO_RGB(0, 0, 0));
	delay();
	rec.width = 1;
	rec.height = 1;
	for (i = 0; i < 300; i++) {
		rec.origin.x = (rand() * i) % view->width;
		rec.origin.y = (rand() * i) % view->height;
		dlo_fill_rect(&FlashDisk_MS_Interface, view, &rec,
				DLO_RGB(0xFF, 0xFF, 0xFF));
	}

	delay();
	for (i = 0; i < 100; i++) {
		rec.width = (rand() * i) % (view->width / 4);
		rec.height = (rand() * i) % (view->height / 4);
		rec.origin.x = ((rand() * i) % (uint32_t)(1.25 * view->width))
				- (view->width / 8);
		rec.origin.y = ((rand() * i) % (uint32_t)(1.25 * view->height))
				- (view->height / 8);
		dlo_fill_rect(&FlashDisk_MS_Interface, view, &rec,
				DLO_RGB((rand() * i) % 0xFF, (rand() * i) % 0xFF,
						(rand() * i) % 0xFF));
	}

	delay();
	for (i = 0; i < 100; i++) {
		dlo_col32_t col = DLO_RGB((i * 3) % 256, (i * 5) % 256,
				255 - ((i * 7) % 256));

		rec.width = (view->width / MID_REC_DIV) - 2 * i;
		rec.height = (view->height / MID_REC_DIV) - 2 * i;
		rec.origin.x = i + (view->width / 2) - (view->width / MID_REC_DIV / 2);
		rec.origin.y = i + (view->height / 2)
				- (view->height / MID_REC_DIV / 2);
		dlo_fill_rect(&FlashDisk_MS_Interface, view, &rec, col);
	}
}

/* Function to spin forever when there is an error */
static void die(FRESULT rc) {
#if 0
	DEBUGOUT("*******DIE %d*******\r\n", rc);
	while (1) {}/* Spin for ever */
#endif
}

/** Configures the board hardware and chip peripherals for the demo's functionality. */
static void SetupHardware(void) {
	SystemCoreClockUpdate();
	Board_Init();
#if (defined(CHIP_LPC43XX) || defined(CHIP_LPC18XX))
	if (FlashDisk_MS_Interface.Config.PortNumber == 0) {
		Chip_USB0_Init();
	} else {
		Chip_USB1_Init();
	}
#endif
	USB_Init(FlashDisk_MS_Interface.Config.PortNumber, USB_MODE_Host);
	/* Hardware Initialization */
	Board_Debug_Init();
}

/* Function to do the read/write to USB Disk */
static void USB_ReadWriteFile(void) {
	FRESULT rc; /* Result code */
	int i;
	UINT bw, br;
	uint8_t *ptr;
	char debugBuf[64];
	DIR dir; /* Directory object */
	FILINFO fno; /* File information object */

	f_mount(0, &fatFS); /* Register volume work area (never fails) */

	rc = f_open(&fileObj, "MESSAGE.TXT", FA_READ);
	if (rc) {
		DEBUGOUT("Unable to open MESSAGE.TXT from USB Disk\r\n");
		die(rc);
	} else {
		DEBUGOUT("Opened file MESSAGE.TXT from USB Disk. Printing contents...\r\n\r\n");
		for (;;) {
			/* Read a chunk of file */
			rc = f_read(&fileObj, buffer, sizeof buffer, &br);
			if (rc || !br) {
				break; /* Error or end of file */
			}
			ptr = (uint8_t *) buffer;
			for (i = 0; i < br; i++) { /* Type the data */
				DEBUGOUT("%c", ptr[i]);
			}
		}
		if (rc) {
			die(rc);
		}

		DEBUGOUT("\r\n\r\nClose the file.\r\n");
		rc = f_close(&fileObj);
		if (rc) {
			die(rc);
		}
	}

	DEBUGOUT("\r\nCreate a new file (hello.txt).\r\n");
	rc = f_open(&fileObj, "HELLO.TXT", FA_WRITE | FA_CREATE_ALWAYS);
	if (rc) {
		die(rc);
	} else {

		DEBUGOUT("\r\nWrite a text data. (Hello world!)\r\n");

		rc = f_write(&fileObj, "Hello world!\r\n", 14, &bw);
		if (rc) {
			die(rc);
		} else {
			sprintf(debugBuf, "%u bytes written.\r\n", bw);
			DEBUGOUT(debugBuf);
		}DEBUGOUT("\r\nClose the file.\r\n");
		rc = f_close(&fileObj);
		if (rc) {
			die(rc);
		}
	}DEBUGOUT("\r\nOpen root directory.\r\n");
	rc = f_opendir(&dir, "");
	if (rc) {
		die(rc);
	} else {
		DEBUGOUT("\r\nDirectory listing...\r\n");
		for (;;) {
			/* Read a directory item */
			rc = f_readdir(&dir, &fno);
			if (rc || !fno.fname[0]) {
				break; /* Error or end of dir */
			}
			if (fno.fattrib & AM_DIR) {
				sprintf(debugBuf, "   <dir>  %s\r\n", fno.fname);
			} else {
				sprintf(debugBuf, "   %8lu  %s\r\n", fno.fsize, fno.fname);
			}DEBUGOUT(debugBuf);
		}
		if (rc) {
			die(rc);
		}
	}DEBUGOUT("\r\nTest completed.\r\n");
	//USB_Host_SetDeviceConfiguration(FlashDisk_MS_Interface.Config.PortNumber,0);
}

/*****************************************************************************
 * Public functions
 ****************************************************************************/

/** Main program entry point. This routine configures the hardware required by the application, then
 *  calls the filesystem function to read files from USB Disk
 */
int main(void) {
	SetupHardware();

	DEBUGOUT("Mass Storage Host Demo running.\r\n");

	USB_ReadWriteFile();
	testBall();

	delay();
	DEBUGOUT("Example completed.\r\n");
	while (1) {
	}
}

/** Event handler for the USB_DeviceAttached event. This indicates that a device has been attached to the host, and
 *  starts the library USB task to begin the enumeration and USB management process.
 */
void EVENT_USB_Host_DeviceAttached(const uint8_t corenum) {
	DEBUGOUT(("Device Attached on port %d\r\n"), corenum);
}

/** Event handler for the USB_DeviceUnattached event. This indicates that a device has been removed from the host, and
 *  stops the library USB task management process.
 */
void EVENT_USB_Host_DeviceUnattached(const uint8_t corenum) {
	DEBUGOUT(("\r\nDevice Unattached on port %d\r\n"), corenum);
}

/** Event handler for the USB_DeviceEnumerationComplete event. This indicates that a device has been successfully
 *  enumerated by the host and is now ready to be used by the application.
 */
void EVENT_USB_Host_DeviceEnumerationComplete(const uint8_t corenum) {
	uint16_t ConfigDescriptorSize;
	uint8_t ConfigDescriptorData[512];

	if (USB_Host_GetDeviceConfigDescriptor(corenum, 1, &ConfigDescriptorSize,
			ConfigDescriptorData, sizeof(ConfigDescriptorData))
			!= HOST_GETCONFIG_Successful) {
		DEBUGOUT("Error Retrieving Configuration Descriptor.\r\n");
		return;
	}

	//dev.DL_usb_interface=&FlashDisk_MS_Interface;
	dev.buffer = buffData;
	FlashDisk_MS_Interface.dev = &dev;
	FlashDisk_MS_Interface.Config.PortNumber = corenum;
	if (DL_Host_ConfigurePipes(&FlashDisk_MS_Interface, ConfigDescriptorSize,
			ConfigDescriptorData) != MS_ENUMERROR_NoError) {
		DEBUGOUT("Attached Device Not a Valid Mass Storage Device.\r\n");
		return;
	}

	if (USB_Host_SetDeviceConfiguration(
			FlashDisk_MS_Interface.Config.PortNumber, 1)
			!= HOST_SENDCONTROL_Successful) {
		DEBUGOUT("Error Setting Device Configuration.\r\n");
		return;
	}

	if (DL_Host_GetEdid(&FlashDisk_MS_Interface, &dev)) {
		DEBUGOUT("Error retrieving max LUN index.\r\n");
		USB_Host_SetDeviceConfiguration(
				FlashDisk_MS_Interface.Config.PortNumber, 0);
		return;
	}

	DEBUGOUT("Mass Storage Device Enumerated.\r\n");
}

/** Event handler for the USB_HostError event. This indicates that a hardware error occurred while in host mode. */
void EVENT_USB_Host_HostError(const uint8_t corenum, const uint8_t ErrorCode) {
	USB_Disable(corenum, USB_MODE_Host);

	DEBUGOUT(("Host Mode Error\r\n"
					" -- Error port %d\r\n"
					" -- Error Code %d\r\n" ), corenum, ErrorCode);

	for (;;) {
	}
}

/** Event handler for the USB_DeviceEnumerationFailed event. This indicates that a problem occurred while
 *  enumerating an attached USB device.
 */
void EVENT_USB_Host_DeviceEnumerationFailed(const uint8_t corenum,
		const uint8_t ErrorCode, const uint8_t SubErrorCode) {
	DEBUGOUT(("Dev Enum Error\r\n"
					" -- Error port %d\r\n"
					" -- Error Code %d\r\n"
					" -- Sub Error Code %d\r\n"
					" -- In State %d\r\n" ),
			corenum, ErrorCode, SubErrorCode, USB_HostState[corenum]);

}

/* Get the disk data structure */
DISK_HANDLE_T *FSUSB_DiskInit(void) {
	return &FlashDisk_MS_Interface;
}

/* Wait for disk to be inserted */
int FSUSB_DiskInsertWait(DISK_HANDLE_T *hDisk) {
	while (USB_HostState[hDisk->Config.PortNumber] != HOST_STATE_Configured) {
		//MS_Host_USBTask(hDisk);
		USB_USBTask(hDisk->Config.PortNumber, USB_MODE_Host);
	}
	return 1;
}

/* Disk acquire function that waits for disk to be ready */
int FSUSB_DiskAcquire(DISK_HANDLE_T *hDisk) {

	return 1;
}

/* Get sector count */
uint32_t FSUSB_DiskGetSectorCnt(DISK_HANDLE_T *hDisk) {
	return DiskCapacity.Blocks;
}

/* Get Block size */
uint32_t FSUSB_DiskGetSectorSz(DISK_HANDLE_T *hDisk) {
	return DiskCapacity.BlockSize;
}

/* Read sectors */
int FSUSB_DiskReadSectors(DISK_HANDLE_T *hDisk, void *buff, uint32_t secStart,
		uint32_t numSec) {
	return 1;
}

/* Write Sectors */
int FSUSB_DiskWriteSectors(DISK_HANDLE_T *hDisk, void *buff, uint32_t secStart,
		uint32_t numSec) {

	return 1;
}

/* Disk ready function */
int FSUSB_DiskReadyWait(DISK_HANDLE_T *hDisk, int tout) {
	volatile int i = tout * 100;
	while (i--) { /* Just delay */
	}
	return 1;
}
