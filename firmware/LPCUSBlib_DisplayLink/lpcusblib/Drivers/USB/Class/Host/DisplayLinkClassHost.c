#define  __INCLUDE_FROM_USB_DRIVER
#include "../../Core/USBMode.h"

#if defined(USB_CAN_BE_HOST)

#define  __INCLUDE_FROM_DL_DRIVER
#define  __INCLUDE_FROM_DISPLAYLINK_HOST_C
#include "DisplayLinkClassHost.h"
#include "../Common/dlo_structs.h"
#include "../Common/dlo_mode.h"

#define STD_CHANNEL "\x57\xCD\xDC\xA7\x1C\x88\x5E\x15\x60\xFE\xC6\x97\x16\x3D\x47\xF2"
#define WRITE_TIMEOUT (10000u)

static uint16_t byteInBuf = 0;

uint8_t DL_Host_ConfigurePipes(USB_ClassInfo_MS_Host_t* const MSInterfaceInfo,
		uint16_t ConfigDescriptorSize, void* ConfigDescriptorData) {
	USB_Descriptor_Endpoint_t* DataINEndpoint = NULL;
	USB_Descriptor_Endpoint_t* DataOUTEndpoint = NULL;
	USB_Descriptor_Endpoint_t* DataOUT2Endpoint = NULL;
	USB_Descriptor_Interface_t* MassStorageInterface = NULL;
	uint8_t portnum = MSInterfaceInfo->Config.PortNumber;
	uint8_t contInt = 1;

	memset(&MSInterfaceInfo->State, 0x00, sizeof(MSInterfaceInfo->State));

	if (DESCRIPTOR_TYPE(ConfigDescriptorData) != DTYPE_Configuration)
		return MS_ENUMERROR_InvalidConfigDescriptor;

	while (!(DataINEndpoint) || !(DataOUTEndpoint) || !(DataOUT2Endpoint)) {
		if (!(MassStorageInterface)
				|| USB_GetNextDescriptorComp(&ConfigDescriptorSize,
						&ConfigDescriptorData,
						DCOMP_DL_Host_NextMSInterfaceEndpoint)
						!= DESCRIPTOR_SEARCH_COMP_Found) {
			if (USB_GetNextDescriptorComp(&ConfigDescriptorSize,
					&ConfigDescriptorData, DCOMP_DL_Host_NextMSInterface)
					!= DESCRIPTOR_SEARCH_COMP_Found) {
				return MS_ENUMERROR_NoCompatibleInterfaceFound;
			}

			MassStorageInterface = DESCRIPTOR_PCAST(ConfigDescriptorData,
					USB_Descriptor_Interface_t);

			DataINEndpoint = NULL;
			DataOUTEndpoint = NULL;
			DataOUT2Endpoint = NULL;
			continue;
		}

		USB_Descriptor_Endpoint_t* EndpointData = DESCRIPTOR_PCAST(
				ConfigDescriptorData, USB_Descriptor_Endpoint_t);

		if ((EndpointData->EndpointAddress & ENDPOINT_DIR_MASK)
				== ENDPOINT_DIR_IN)
			DataINEndpoint = EndpointData;
		else {
			if (contInt == 1)
				DataOUTEndpoint = EndpointData;
			else if (contInt == 2)
				DataOUT2Endpoint = EndpointData;
			contInt++;
		}

	}

	for (uint8_t PipeNum = 1; PipeNum < PIPE_TOTAL_PIPES; PipeNum++) {
		uint16_t Size;
		uint8_t Type;
		uint8_t Token;
		uint8_t EndpointAddress;
		bool DoubleBanked;

		if (PipeNum == MSInterfaceInfo->Config.DataINPipeNumber) {
			Size = le16_to_cpu(DataINEndpoint->EndpointSize);
			EndpointAddress = DataINEndpoint->EndpointAddress;
			Token = PIPE_TOKEN_IN;
			Type = EP_TYPE_INTERRUPT;
			DoubleBanked = MSInterfaceInfo->Config.DataINPipeDoubleBank;

			MSInterfaceInfo->State.DataINPipeSize =
					DataINEndpoint->EndpointSize;
		} else if (PipeNum == MSInterfaceInfo->Config.DataOUTPipeNumber) {
			Size = le16_to_cpu(DataOUTEndpoint->EndpointSize);
			EndpointAddress = DataOUTEndpoint->EndpointAddress;
			Token = PIPE_TOKEN_OUT;
			Type = EP_TYPE_BULK;
			DoubleBanked = MSInterfaceInfo->Config.DataOUTPipeDoubleBank;

			MSInterfaceInfo->State.DataOUTPipeSize =
					DataOUTEndpoint->EndpointSize;
		} else if (PipeNum == MSInterfaceInfo->Config.DataOUT2PipeNumber) {
			Size = le16_to_cpu(DataOUT2Endpoint->EndpointSize);
			EndpointAddress = DataOUT2Endpoint->EndpointAddress;
			Token = PIPE_TOKEN_OUT;
			Type = EP_TYPE_BULK;
			DoubleBanked = MSInterfaceInfo->Config.DataOUT2PipeDoubleBank;

			MSInterfaceInfo->State.DataOUT2PipeSize =
					DataOUT2Endpoint->EndpointSize;
		} else {
			continue;
		}

		if (!(Pipe_ConfigurePipe(portnum, PipeNum, Type, Token, EndpointAddress,
				Size, DoubleBanked ? PIPE_BANK_DOUBLE : PIPE_BANK_SINGLE))) {
			return MS_ENUMERROR_PipeConfigurationFailed;
		}
	}

	MSInterfaceInfo->State.InterfaceNumber =
			MassStorageInterface->InterfaceNumber;
	MSInterfaceInfo->State.IsActive = true;

	return MS_ENUMERROR_NoError;
}

static uint8_t DCOMP_DL_Host_NextMSInterface(void* const CurrentDescriptor) {
	USB_Descriptor_Header_t* Header = DESCRIPTOR_PCAST(CurrentDescriptor,
			USB_Descriptor_Header_t);

	if (Header->Type == DTYPE_Interface) {
		USB_Descriptor_Interface_t* Interface = DESCRIPTOR_PCAST(
				CurrentDescriptor, USB_Descriptor_Interface_t);

		if ((Interface->Class == MS_CSCP_MassStorageClass)
				&& (Interface->SubClass == MS_CSCP_SCSITransparentSubclass)
				&& (Interface->Protocol == MS_CSCP_BulkOnlyTransportProtocol)) {
			return DESCRIPTOR_SEARCH_Found;
		}
	}

	return DESCRIPTOR_SEARCH_NotFound;
}

static uint8_t DL_Host_SendBulk(USB_ClassInfo_MS_Host_t* const MSInterfaceInfo,
		void * const Buffer, uint16_t Length, uint8_t forceFlush) {
	uint8_t ErrorCode = PIPE_RWSTREAM_NoError;
	uint8_t portnum = MSInterfaceInfo->Config.PortNumber;

//	if (++MSInterfaceInfo->State.TransactionTag == 0xFFFFFFFF)
//		MSInterfaceInfo->State.TransactionTag = 1;

//	if (forceFlush || byteInBuf >= PIPE_MAX_SIZE) {
//		Pipe_ClearOUT(portnum);
//		Pipe_WaitUntilReady(portnum);
//		byteInBuf = 0;
//	}

	Pipe_SelectPipe(portnum, MSInterfaceInfo->Config.DataOUTPipeNumber);
	Pipe_Unfreeze();

	if ((ErrorCode = Pipe_Write_Stream_LE(portnum, Buffer, Length,
	NULL)) != PIPE_RWSTREAM_NoError) {
		return ErrorCode;
	}

	Pipe_ClearOUT(portnum);
	Pipe_WaitUntilReady(portnum);
//	Pipe_Freeze();

//	if (BufferPtr != NULL)
//	{
////		ErrorCode = MS_Host_SendReceiveData(MSInterfaceInfo, SCSICommandBlock, (void*)BufferPtr);
////
////		if ((ErrorCode != PIPE_RWSTREAM_NoError) && (ErrorCode != PIPE_RWSTREAM_PipeStalled))
////		{
////			Pipe_Freeze();
////			return ErrorCode;
////		}
//	}

	return PIPE_RWSTREAM_NoError;
}

static uint8_t DCOMP_DL_Host_NextMSInterfaceEndpoint(
		void* const CurrentDescriptor) {
	USB_Descriptor_Header_t* Header = DESCRIPTOR_PCAST(CurrentDescriptor,
			USB_Descriptor_Header_t);

	if (Header->Type == DTYPE_Endpoint) {
		USB_Descriptor_Endpoint_t* Endpoint = DESCRIPTOR_PCAST(
				CurrentDescriptor, USB_Descriptor_Endpoint_t);

		uint8_t EndpointType = (Endpoint->Attributes & EP_TYPE_MASK);

		if (((EndpointType == EP_TYPE_BULK)
				|| (EndpointType == EP_TYPE_INTERRUPT))
				&& (!(Pipe_IsEndpointBound(Endpoint->EndpointAddress)))) {
			return DESCRIPTOR_SEARCH_Found;
		}
	} else if (Header->Type == DTYPE_Interface) {
		return DESCRIPTOR_SEARCH_Fail;
	}

	return DESCRIPTOR_SEARCH_NotFound;
}

uint8_t DL_Host_GetEdid(USB_ClassInfo_MS_Host_t* const MSInterfaceInfo,
		dlo_device_t * const dev) {
	uint8_t edid[EDID_STRUCT_SZ];
	uint8_t buf[2];
	uint8_t ErrorCode;
	uint8_t portnum = MSInterfaceInfo->Config.PortNumber;
	uint32_t i;

	dev->claimed = true;

	/* Allocate a buffer to hold commands before they are sent to the device */

	dev->bufptr = dev->buffer;
	dev->bufend = dev->buffer + BUF_TAM;

	//DPRINTF("usb: open: buffer &%X, &%X, &%X\n", (int)dev->buffer, (int)dev->bufptr, (int)dev->bufend);

	/* Use the default timeout if none was specified */
	if (!dev->timeout)
		dev->timeout = WRITE_TIMEOUT;
	//DPRINTF("usb: open: timeout %u ms\n", dev->timeout);

	/* Initialise the supported modes array for this device to include all our pre-defined modes */
	use_default_modes(dev);

	for (i = 0; i < EDID_STRUCT_SZ; i++) {
		USB_ControlRequest = (USB_Request_Header_t )
				{ .bmRequestType = (REQTYPE_VENDOR | REQDIR_DEVICETOHOST),
						.bRequest = NR_USB_REQUEST_I2C_SUB_IO, .wValue = i << 8,
						.wIndex = 0xA1, .wLength = sizeof(buf), };

		Pipe_SelectPipe(portnum, PIPE_CONTROLPIPE);

		if ((ErrorCode = USB_Host_SendControlRequest(portnum, buf))
				== HOST_SENDCONTROL_SetupStalled) {
			ErrorCode = HOST_SENDCONTROL_Successful;
		}

		if (buf[0]) {
			ErrorCode = HOST_SENDCONTROL_PipeError;
		}
		edid[i] = buf[1];
	}

	dlo_mode_parse_edid(dev, &edid, EDID_STRUCT_SZ);
	dlo_mode_set_default(MSInterfaceInfo, 0);
	return ErrorCode;
}

dlo_retcode_t dlo_usb_std_chan(USB_ClassInfo_MS_Host_t* const MSInterfaceInfo) {
	dlo_retcode_t err;

	ASSERT(strlen(STD_CHANNEL) == 16);
	err = dlo_usb_chan_sel(MSInterfaceInfo, STD_CHANNEL, DSIZEOF(STD_CHANNEL));

	return err;
}
dlo_retcode_t dlo_usb_chan_sel(USB_ClassInfo_MS_Host_t* const MSInterfaceInfo,
		const char * const buf, const uint16_t size) {
	uint8_t ErrorCode;
	uint8_t portnum = MSInterfaceInfo->Config.PortNumber;
	if (size) {
		USB_ControlRequest = (USB_Request_Header_t )
				{ .bmRequestType = REQTYPE_VENDOR, .bRequest =
				NR_USB_REQUEST_CHANNEL, .wValue = 0, .wIndex = 0, .wLength =
						size, };

		Pipe_SelectPipe(portnum, PIPE_CONTROLPIPE);

		if ((ErrorCode = USB_Host_SendControlRequest(portnum, buf))
				== HOST_SENDCONTROL_SetupStalled) {
			ErrorCode = HOST_SENDCONTROL_Successful;
		}

	}

	return dlo_ok;
}

dlo_retcode_t dlo_usb_write(USB_ClassInfo_MS_Host_t* const MSInterfaceInfo,
		uint8_t forceFlush) {
	dlo_device_t * dev = MSInterfaceInfo->dev;
	dlo_retcode_t err = dlo_usb_write_buf(MSInterfaceInfo, dev->buffer,
			dev->bufptr - dev->buffer, forceFlush);

	dev->bufptr = dev->buffer;

	return err;
}

dlo_retcode_t dlo_usb_write_buf(USB_ClassInfo_MS_Host_t* const MSInterfaceInfo,
		char * buf, uint16_t size, uint8_t forceFlush) {

	uint8_t ErrorCode = PIPE_RWSTREAM_NoError;

	if (!size)
		return dlo_ok;

	while (size) {
		uint16_t num = size > PIPE_MAX_SIZE ? PIPE_MAX_SIZE : size;

		ErrorCode = DL_Host_SendBulk(MSInterfaceInfo, buf, num, forceFlush);

		buf += num;
		size -= num;
	}
	return dlo_ok;
}

#endif

