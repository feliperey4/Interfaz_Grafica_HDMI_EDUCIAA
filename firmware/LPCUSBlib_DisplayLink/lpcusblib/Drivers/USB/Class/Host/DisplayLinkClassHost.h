/*
 * @brief Host mode driver for the library USB Mass Storage Class driver
 *
 * @note
 * Copyright(C) NXP Semiconductors, 2012
 * Copyright(C) Dean Camera, 2011, 2012
 * All rights reserved.
 *
 * @par
 * Software that is described herein is for illustrative purposes only
 * which provides customers with programming information regarding the
 * LPC products.  This software is supplied "AS IS" without any warranties of
 * any kind, and NXP Semiconductors and its licensor disclaim any and
 * all warranties, express or implied, including all implied warranties of
 * merchantability, fitness for a particular purpose and non-infringement of
 * intellectual property rights.  NXP Semiconductors assumes no responsibility
 * or liability for the use of the software, conveys no license or rights under any
 * patent, copyright, mask work right, or any other intellectual property rights in
 * or to any products. NXP Semiconductors reserves the right to make changes
 * in the software without notification. NXP Semiconductors also makes no
 * representation or warranty that such application will be suitable for the
 * specified use without further testing or modification.
 *
 * @par
 * Permission to use, copy, modify, and distribute this software and its
 * documentation is hereby granted, under NXP Semiconductors' and its
 * licensor's relevant copyrights in the software, without fee, provided that it
 * is used in conjunction with NXP Semiconductors microcontrollers.  This
 * copyright, permission, and disclaimer notice must appear in all copies of
 * this code.
 */

/** @ingroup Group_USBClassMS
 *  @defgroup Group_USBClassMassStorageHost Mass Storage Class Host Mode Driver
 *
 *  @section Sec_Dependencies Module Source Dependencies
 *  The following files must be built with any user project that uses this module:
 *    - LPCUSBlib/Drivers/USB/Class/Host/MassStorage.c <i>(Makefile source module name: LPCUSBlib_SRC_USBCLASS)</i>
 *
 *  @section Sec_ModDescription Module Description
 *  Host Mode USB Class driver framework interface, for the Mass Storage USB Class driver.
 *
 *  @{
 */

#ifndef __DL_CLASS_HOST_H__
#define __DL_CLASS_HOST_H__

/* Includes: */
#include "../../USB.h"
#include "../Common/DisplayLinkClassCommon.h"
#include "../Common/dlo_structs.h"
#include "../Common/libdlo.h"

/* Enable C linkage for C++ Compilers: */
#if defined(__cplusplus)
extern "C" {
#endif

/* Preprocessor Checks: */
#if !defined(__INCLUDE_FROM_DL_DRIVER)
#error Do not include this file directly. Include LPCUSBlib/Drivers/USB.h instead.
#endif

/* Public Interface - May be used in end-application: */
/* Macros: */
/** Error code for some Mass Storage Host functions, indicating a logical (and not hardware) error. */
#define MS_ERROR_LOGICAL_CMD_FAILED              0x80

#define BUF_TAM (1*1024u)

#define FORCE_FLUSH 1
#define AUTO_FUSH 0

/* Type Defines: */
/** @brief Mass Storage Class Host Mode Configuration and State Structure.
 *
 *  Class state structure. An instance of this structure should be made within the user application,
 *  and passed to each of the Mass Storage class driver functions as the \c MSInterfaceInfo parameter. This
 *  stores each Mass Storage interface's configuration and state information.
 */

/** @brief SCSI Device LUN Capacity Structure.
 *
 *  SCSI capacity structure, to hold the total capacity of the device in both the number
 *  of blocks in the current LUN, and the size of each block. This structure is filled by
 *  the device when the @ref MS_Host_ReadDeviceCapacity() function is called.
 */
typedef struct {
	uint32_t Blocks; /**< Number of blocks in the addressed LUN of the device. */
	uint32_t BlockSize; /**< Number of bytes in each block in the addressed LUN. */
} SCSI_Capacity_t;

/* Enums: */
enum MS_Host_EnumerationFailure_ErrorCodes_t {
	MS_ENUMERROR_NoError = 0, /**< Configuration Descriptor was processed successfully. */
	MS_ENUMERROR_InvalidConfigDescriptor = 1, /**< The device returned an invalid Configuration Descriptor. */
	MS_ENUMERROR_NoCompatibleInterfaceFound = 2, /**< A compatible Mass Storage interface was not found in the device's Configuration Descriptor. */
	MS_ENUMERROR_PipeConfigurationFailed = 3, /**< One or more pipes for the specified interface could not be configured correctly. */
};

uint8_t DL_Host_ConfigurePipes(USB_ClassInfo_MS_Host_t* const MSInterfaceInfo,
		uint16_t ConfigDescriptorSize, void* ConfigDescriptorData)
ATTR_NON_NULL_PTR_ARG(1) ATTR_NON_NULL_PTR_ARG(3);

uint8_t DL_Host_GetEdid(USB_ClassInfo_MS_Host_t* const MSInterfaceInfo,
		dlo_device_t * const dev);

dlo_retcode_t dlo_usb_chan_sel(USB_ClassInfo_MS_Host_t* const MSInterfaceInfo,
		const char * const buf, const uint16_t size);

dlo_retcode_t dlo_usb_std_chan(USB_ClassInfo_MS_Host_t* const MSInterfaceInfo);

dlo_retcode_t dlo_usb_write_buf(USB_ClassInfo_MS_Host_t* const MSInterfaceInfo,
		char * buf, uint16_t size, uint8_t forceFlush);

dlo_retcode_t dlo_usb_write(USB_ClassInfo_MS_Host_t* const MSInterfaceInfo,
		uint8_t forceFlush);

static uint8_t DL_Host_SendBulk(USB_ClassInfo_MS_Host_t* const MSInterfaceInfo,
		void * const Buffer, uint16_t Length, uint8_t forceFlush);

/* Private Interface - For use in library only: */
#if !defined(__DOXYGEN__)
/* Macros: */
#define MS_COMMAND_DATA_TIMEOUT_MS        10000

/* Function Prototypes: */
#if defined(__INCLUDE_FROM_DISPLAYLINK_HOST_C)

static uint8_t DCOMP_DL_Host_NextMSInterface(void* const CurrentDescriptor)
ATTR_WARN_UNUSED_RESULT ATTR_NON_NULL_PTR_ARG(1);
static uint8_t DCOMP_DL_Host_NextMSInterfaceEndpoint(void* const CurrentDescriptor)
ATTR_WARN_UNUSED_RESULT ATTR_NON_NULL_PTR_ARG(1);
#endif
#endif

/* Disable C linkage for C++ Compilers: */
#if defined(__cplusplus)
}
#endif

#endif

/** @} */

