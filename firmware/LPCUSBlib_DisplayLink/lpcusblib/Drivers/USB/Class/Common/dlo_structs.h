/** @file dlo_structs.h
 *
 *  @brief This file defines all of the internal structures used by libdlo.
 *
 *  DisplayLink Open Source Software (libdlo)
 *  Copyright (C) 2009, DisplayLink
 *  www.displaylink.com
 *
 *  This library is free software; you can redistribute it and/or modify it under
 *  the terms of the GNU Library General Public License as published by the Free
 *  Software Foundation; LGPL version 2, dated June 1991.
 *
 *  This library is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this library; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef DLO_STRUCTS_H
#define DLO_STRUCTS_H        /**< Avoid multiple inclusion. */

#include "dlo_data.h"
#include <stdint.h>
#include <stdbool.h>
//#include "../Host/DisplayLinkClassHost.h"
//#include "../../USB.h"

//typedef uint8_t bool;

/** Structure holding all of the information specific to a particular device.
 */
//typedef struct dlo_device_s dlo_device_t;


/** An pointer to a location in the DisplayLink device logical memory map. */
typedef uint32_t dlo_ptr_t;

/** Flags word to configure the @c dlo_copy_host_bmp() call. */
typedef struct dlo_bmpflags_s
{
  unsigned v_flip :1;        /**< Vertically flip the bitmap during the copy. */
} dlo_bmpflags_t;            /**< A struct @a dlo_bmpflags_s. */


/** A 32 bits per pixel colour number (0x00bbggrr, little endian). Note: most significant byte is undefined. */
typedef uint32_t dlo_col32_t;

 /** An opaque device handle. */
 typedef void *dlo_dev_t;




/** Co-ordinates of a point/pixel relative to the origin, top-left, of a given @a dlo_view_t. */
typedef struct dlo_dot_s
{
  int32_t x;                 /**< X co-ordinate of the dot, pixels from left edge of screen (+ve is right). */
  int32_t y;                 /**< Y co-ordinate of the dot, pixels down from top edge of screen (-ve is up). */
} dlo_dot_t;                 /**< A struct @a dlo_dot_s. */


/** A rectangle relative to a given @a dlo_view_t. */
typedef struct dlo_rect_s
{
  dlo_dot_t origin;          /**< Origin co-ordinates (top-left of the rectangle). */
  uint16_t  width;           /**< Width (pixels) of rectangle (all pixels from origin.x to origin.x+width-1 inclusive). */
  uint16_t  height;          /**< Height (pixels) of rectangle (all pixels from origin.y to origin.x+height-1 inclusive). */
} dlo_rect_t;                /**< A struct @a dlo_rect_s. */


/** Established timing information, derived from EDID.
 */
typedef struct est_timing_s
{
  uint16_t width;        /**< Width of mode (pixels). */
  uint16_t height;       /**< Height of mode (pixels). */
  uint8_t  refresh;      /**< Mode refresh rate (Hz). */
} est_timing_t;          /**< A struct @a est_timings_s. */


/** Vendor/product information.
 */
typedef struct edid_prod_id_s
{
  uint16_t manuf_name;   /**< ID manufacturer code. */
  uint16_t prod_code;    /**< ID product code. */
  uint32_t serial_num;   /**< ID serial number. */
  uint8_t  manuf_wk;     /**< Week of manufacture. */
  uint8_t  manuf_yr;     /**< Year of manufacture. */
} edid_prod_id_t;        /**< A struct @a edid_prod_id_s. */


/** EDID structure information.
 */
typedef struct edid_struct_vsn_s
{
  uint8_t number;        /**< Version number. */
  uint8_t revision;      /**< Revision number. */
} edid_struct_vsn_t;     /**< A struct @a edid_struct_vsn_s. */


/** Basic dislpay parameters/features.
 */
typedef struct edid_basic_params_s
{
  uint8_t input_def;     /**< Video input definition. */
  uint8_t max_horiz_sz;  /**< Maximum horizontal image size (cm). */
  uint8_t max_vert_sz;   /**< Maximum vertical image size (cm). */
  float   gamma;         /**< Display transfer characteristic (gamma). */
  uint8_t features;      /**< Feature support. */
} edid_basic_params_t;   /**< A struct @a edid_basic_params_s. */


/** Colour characteristics.
 */
typedef struct edid_colours_s
{
  uint16_t red_grn_low;  /**< Red/green low bits. */
  uint16_t blu_wht_low;  /**< Blue/white low bits. */
  uint16_t red_x;        /**< Red-x (bits 9-2). */
  uint16_t red_y;        /**< Red-y (bits 9-2). */
  uint16_t grn_x;        /**< Green-x (bits 9-2). */
  uint16_t grn_y;        /**< Green-y (bits 9-2). */
  uint16_t blu_x;        /**< Blue-x (bits 9-2). */
  uint16_t blu_y;        /**< Blue-y (bits 9-2). */
  uint16_t wht_x;        /**< White-x (bits 9-2). */
  uint16_t wht_y;        /**< White-y (bits 9-2). */
} edid_colours_t;        /**< A struct @a edid_colours_s. */


/** Established timings.
 */
typedef struct edid_est_timings_s
{
  uint8_t timings[2];    /**< Bitfields of established timings. */
  uint8_t resvd;         /**< Manufacturer's reserved timings. */
} edid_est_timings_t;    /**< A struct @a edid_est_timings_s. */


/** Standard timing identification.
 */
typedef struct edid_std_timing_s
{
  uint16_t timing_id[8];  /**< Standard timing identification. */
} edid_std_timing_t;      /**< A struct @a edid_std_timing_s. */


/** Standard EDID Detailed Timing Block, in unpacked form.
 ** There are 4 of them (18 bytes in packed form) 
 ** in each 128 byte EDID returned from the monitor.
 ** The first of those 4 is the preferred mode for the monitor.
 **
 ** Only the packed form is called out in the standard. This
 ** unpacked form is just for convenience, since the packed
 ** form scatters low and higher order bits all over.
 **
 ** Units for all items are pixels (horizontal), 
 ** or lines (vertical), unless otherwise called out
 */ 
typedef struct edid_detail_unpacked_s 
{
  uint16_t pixelClock10KHz; /**< in 10kHz units */
  uint16_t hActive;
  uint16_t hBlanking;
  uint16_t vActive;
  uint16_t vBlanking;
  uint16_t hSyncOffset;
  uint16_t hSyncPulseWidth;
  uint16_t vSyncOffset;
  uint16_t vSyncPulseWidth;
  uint16_t hImageSizeMm;    /**< in millimeters */
  uint16_t vImageSizeMm;    /**< in millimeters */
  uint8_t  hBorder;
  uint8_t  vBorder;
  uint8_t  bInterlaced;
  uint8_t  bStereo;
  uint8_t  bSeparateSync;
  uint8_t  bVSyncPositive;
  uint8_t  bHSyncPositive;
  uint8_t  bStereoMode;
} edid_detail_unpacked_t;

/** An EDID extension block.
 */
typedef struct edid_ext_block_s
{
  uint8_t unknown[128];  /**< Contents of block are unknown. */
} edid_ext_block_t;      /**< A struct @a edid_ext_block_s. */

/** Struture holding parsed EDID information
 */ 
typedef struct dlo_edid_s
{
  edid_prod_id_t      product;      /**< Vendor/product information. */
  edid_struct_vsn_t   version;      /**< EDID structure information. */
  edid_basic_params_t basic;        /**< Basic dislpay parameters/features. */
  edid_colours_t      colours;      /**< Colour characteristics. */
  edid_est_timings_t  est_timings;  /**< Established timings. */
  edid_std_timing_t   std_timings;  /**< Standard timing identification. */
  edid_detail_unpacked_t timings[4];/**< Detailed timing descriptions. */
  uint8_t             ext_blocks;   /**< Number of extension blocks. */
} dlo_edid_t;                    


/** A mode number used to index a specific mode from the list defined in dlo_mode_data.c.
 */
typedef uint32_t dlo_modenum_t;

/** An pointer to a location in the DisplayLink device logical memory map. */
typedef uint32_t dlo_ptr_t;


typedef struct dlo_view_s
{
  uint16_t  width;           /**< Width (pixels). */
  uint16_t  height;          /**< Height (pixels). */
  uint8_t   bpp;             /**< Colour depth (bits per pixel). */
  dlo_ptr_t base;            /**< Base address in the device memory. */
} dlo_view_t;

/** An internal representation of a viewport within the DisplayLink device.
 *
 *  An area is generated from a viewport and a rectangle within that viewport (which
 *  has no parts lying outside but may cover the complete extent of the viewport). It
 *  has a base address for both the 16 bpp component of a pixel's colour and the 8 bpp
 *  fine detail component. It also requires a stride in the case where the rectangle
 *  didn't fully occupy the horizontal extent of the viewport.
 */
typedef struct dlo_area_s
{
  dlo_view_t view;           /**< Viewport information (normalised to a specific rectangle within a viewport). */
  dlo_ptr_t  base8;          /**< The base address of the 8 bpp fine detail colour information. */
  uint32_t   stride;         /**< The stride (pixels) from one pixel in the area to the one directly below. */
} dlo_area_t;                /**< A struct @a dlo_area_s. */


typedef enum
{
  /* Success... */
  dlo_ok = 0u,               /**< Successful. */
  /* Errors... */
  dlo_err_memory = 1u,       /**< A memory claim operation failed; out of memory. */
  dlo_err_bad_area,          /**< An invalid area was specified (e.g. of zero width or height). */
  dlo_err_bad_col,           /**< Unsupported colour depth. */
  dlo_err_bad_device,        /**< Unknown device - has been disconnected or powered down. */
  dlo_err_bad_fbuf,          /**< Null pointer passed as local bitmap data. */
  dlo_err_bad_fmt,           /**< Unsupported bitmap pixel format. */
  dlo_err_bad_mode,          /**< Call to @c set_mode() failed due to unsupported mode parameters. */
  dlo_err_bad_view,          /**< Invalid viewport specified (is screen mode set up?). */
  dlo_err_big_scrape,        /**< Bitmap is too wide for copy buffer.*/
  dlo_err_buf_full,          /**< Command buffer is full. */
  dlo_err_claimed,           /**< Device cannot be claimed - it's already been claimed. */
  dlo_err_edid_fail,         /**< EDID communication with monitor failed. */
  dlo_err_iic_op,            /**< An IIC operation with the device failed. */
  dlo_err_not_root,          /**< Executable should be run as root (e.g. using 'su root' or 'sudo'). */
  dlo_err_open,              /**< Attempt to open a connection to the device failed. */
  dlo_err_overlap,           /**< Source and destination viewports cannot overlap (unless the same). */
  dlo_err_reenum,            /**< Reenumeration required before device can be claimed. */
  dlo_err_unclaimed,         /**< Device cannot be written to: unclaimed. */
  dlo_err_unsupported,       /**< Requested feature is not supported. */
  dlo_err_usb,               /**< A USB-related error: call @c dlo_usb_strerror() for further info. */
  /* Warnings... */
  dlo_warn_dl160_mode = 0x10000000u, /**< This screen mode may not display correctly on DL120 devices. */
  dlo_warn_no_edid_detailed_timing,  /**< EDID descriptor not detailed timing */
  /* User return codes... */
  dlo_user_example = 0x80000000      /**< Return codes 0x80000000 to 0xFFFFFFFF are free for user allocation. */
} dlo_retcode_t;             /**< Return codes. Used to indicate the success or otherwise of a call to the library. */


/** Flags word for the initialisation function (no flags defined at present). */
typedef struct dlo_init_s
{
  unsigned undef :1;         /**< Undefined flag (placeholder). */
} dlo_init_t;                /**< A struct @a dlo_init_s. */


/** Flags word for the finalisation function (no flags defined at present). */
typedef struct dlo_final_s
{
  unsigned undef :1;         /**< Undefined flag (placeholder). */
} dlo_final_t;               /**< A struct @a dlo_final_s. */

/** Descriptor for reading or setting the current screen mode. */
typedef struct dlo_mode_s
{
  dlo_view_t view;           /**< The @a dlo_view_t associated with the screen mode. */
  uint8_t    refresh;        /**< Refresh rate (Hz). */
} dlo_mode_t;                /**< A struct @a dlo_mode_s. */



typedef struct dlo_device_s dlo_device_t;

/** Types of DisplayLink device. */
typedef enum
{
  dlo_dev_unknown = 0,       /**< Unknown device type. */
  dlo_dev_base    = 0xB,     /**< Base platform. */
  dlo_dev_alex    = 0xF,     /**< Alex chipset. */
  dlo_dev_ollie   = 0xF1     /**< Ollie chipset. */
} dlo_devtype_t;             /**< A struct @a dlo_devtype_s. */

/** Structure holding all of the information specific to a particular device.
 */
struct dlo_device_s
{
//  struct USB_ClassInfo_MS_Host_t *DL_usb_interface;
  dlo_device_t  *prev;       /**< Pointer to previous node on device list. */
  dlo_device_t  *next;       /**< Pointer to next node on device list. */
  dlo_devtype_t  type;       /**< Type of DisplayLink device. */
  char          *serial;     /**< Pointer to device serial number string. */
  dlo_edid_t     edid;       /**< Parsed EDID information from device */
  uint8_t           claimed;    /**< Has the device been claimed by someone? */
  uint8_t           check;      /**< Flag is toggled for each enumeration to spot dead nodes in device list. */
  uint32_t       timeout;    /**< Timeout for bulk communications (milliseconds). */
  uint32_t       memory;     /**< Total size of storage in the device (bytes). */
  char          *buffer;     /**< Pointer to the base of the command buffer. */
  char          *bufptr;     /**< Pointer to the first free byte in the command buffer. */
  char          *bufend;     /**< Pointer to the byte after the end byte of the command buffer. */
  //dlo_usb_dev_t *cnct;       /**< Private word for connection specific data or structure pointer. */
  dlo_mode_t     mode;       /**< Current display mode information. */
  dlo_ptr_t      base8;      /**< Pointer to the base of the 8bpp segment (if any). */
  uint8_t           low_blank;  /**< The current raster screen mode has reduced blanking. */
  dlo_mode_t     native;     /**< Mode number of the display's native screen mode (if any). */
  dlo_modenum_t  supported[DLO_MODE_DATA_NUM];  /**< Array of supported mode numbers. */
};


typedef enum
{
  dlo_pixfmt_bgr323   = 0 | DLO_PIXFMT_1BYPP,                   /**< 8 bit per pixel 2_bbbggrrr. */
  dlo_pixfmt_rgb323   = 0 | DLO_PIXFMT_1BYPP | DLO_PIXFMT_SWP,  /**< 8 bit per pixel 2_rrrggbbb. */
  dlo_pixfmt_bgr565   = 1 | DLO_PIXFMT_2BYPP,                   /**< 16 bit per pixel 2_bbbbbggggggrrrrr. */
  dlo_pixfmt_rgb565   = 1 | DLO_PIXFMT_2BYPP | DLO_PIXFMT_SWP,  /**< 16 bit per pixel 2_rrrrrggggggbbbbb. */
  dlo_pixfmt_sbgr1555 = 2 | DLO_PIXFMT_2BYPP,                   /**< 16 bit per pixel 2_Sbbbbbgggggrrrrr (S is supremacy/transparancy bit). */
  dlo_pixfmt_srgb1555 = 2 | DLO_PIXFMT_2BYPP | DLO_PIXFMT_SWP,  /**< 16 bit per pixel 2_Srrrrrgggggbbbbb (S is supremacy/transparancy bit). */
  dlo_pixfmt_bgr888   = 3 | DLO_PIXFMT_3BYPP,                   /**< 24 bit per pixel 0xbbggrr. */
  dlo_pixfmt_rgb888   = 3 | DLO_PIXFMT_3BYPP | DLO_PIXFMT_SWP,  /**< 24 bit per pixel 0xrrggbb. */
  dlo_pixfmt_abgr8888 = 4 | DLO_PIXFMT_4BYPP,                   /**< 32 bit per pixel 0xaabbggrr. */
  dlo_pixfmt_argb8888 = 4 | DLO_PIXFMT_4BYPP | DLO_PIXFMT_SWP   /**< 32 bit per pixel 0xaarrggbb. */
  /* Any value greater than 1023 is assumed to be a pointer to: dlo_col32_t palette[256]
   * for translating paletted 8 bits per pixel data into colour numbers.
   */
} dlo_pixfmt_t;  /**< A struct @a dlo_pixfmt_s. */

/** A local (host-resident) bitmap or framebuffer. */
typedef struct dlo_fbuf_s
{
  uint16_t     width;        /**< Width (pixels). */
  uint16_t     height;       /**< Height (pixels). */
  dlo_pixfmt_t fmt;          /**< Pixel format (e.g. bits per pixel, colour component order, etc). */
  void        *base;         /**< Base address in host memory. */
  uint32_t     stride;       /**< Stride (pixels) from a pixel to the one directly below. */
} dlo_fbuf_t;                /**< A struct @a dlo_fbuf_s. */

typedef struct {
	dlo_device_t *dev;
	struct {
		uint8_t DataINPipeNumber; /**< Pipe number of the Mass Storage interface's IN data pipe. */
		bool DataINPipeDoubleBank; /**< Indicates if the Mass Storage interface's IN data pipe should use double banking. */

		uint8_t DataOUTPipeNumber; /**< Pipe number of the Mass Storage interface's OUT data pipe. */
		bool DataOUTPipeDoubleBank; /**< Indicates if the Mass Storage interface's OUT data pipe should use double banking. */

		uint8_t DataOUT2PipeNumber; /**< Pipe number of the Mass Storage interface's OUT data pipe. */
		bool DataOUT2PipeDoubleBank; /**< Indicates if the Mass Storage interface's OUT data pipe should use double banking. */

		uint8_t PortNumber; /**< Port number that this interface is running.						*/

	} Config; /**< Config data for the USB class interface within the device. All elements in this section
	 *   <b>must</b> be set or the interface will fail to enumerate and operate correctly.
	 */
	struct {
		bool IsActive; /**< Indicates if the current interface instance is connected to an attached device, valid
		 *   after @ref MS_Host_ConfigurePipes() is called and the Host state machine is in the
		 *   Configured state.
		 */
		uint8_t InterfaceNumber; /**< Interface index of the Mass Storage interface within the attached device. */

		uint16_t DataINPipeSize; /**< Size in bytes of the Mass Storage interface's IN data pipe. */
		uint16_t DataOUTPipeSize; /**< Size in bytes of the Mass Storage interface's OUT data pipe. */
		uint16_t DataOUT2PipeSize; /**< Size in bytes of the Mass Storage interface's OUT data pipe. */

		uint32_t TransactionTag; /**< Current transaction tag for data synchronizing of packets. */
	} State; /**< State data for the USB class interface within the device. All elements in this section
	 *   <b>may</b> be set to initial values, but may also be ignored to default to sane values when
	 *   the interface is enumerated.
	 */
} USB_ClassInfo_MS_Host_t;


// Fuentes.
typedef struct
{
  const uint8_t widthBits;              // width, in bits (or pixels), of the character
  const uint16_t offset;                // offset of the character's bitmap, in bytes, into the the FONT_INFO's data array
} FONT_CHAR_INFO;

/**************************************************************************/
/*!
    @brief Describes a single font
*/
/**************************************************************************/
typedef struct
{
  const uint8_t           height;       // height of the font's characters
  const uint8_t           startChar;    // the first character in the font (e.g. in charInfo and data)
  const uint8_t           endChar;      // the last character in the font (e.g. in charInfo and data)
  const FONT_CHAR_INFO*	  charInfo;     // pointer to array of char information
  const uint8_t*          data;         // pointer to generated array of character visual representation
} FONT_INFO;

typedef enum
{
  DRAW_CORNERS_NONE        = 0x00,
  DRAW_CORNERS_TOPLEFT     = 0x01,
  DRAW_CORNERS_TOPRIGHT    = 0x02,
  DRAW_CORNERS_BOTTOMLEFT  = 0x04,
  DRAW_CORNERS_BOTTOMRIGHT = 0x08,
  DRAW_CORNERS_ALL         = 0x0F, // 0x01 + 0x02 + 0x04 + 0x08
  DRAW_CORNERS_TOP         = 0x03, // 0x01 + 0x02
  DRAW_CORNERS_BOTTOM      = 0x0C, // 0x04 + 0x08
  DRAW_CORNERS_LEFT        = 0x05, // 0x01 + 0x04
  DRAW_CORNERS_RIGHT       = 0x0A  // 0x02 + 0x08
} drawCorners_t;

#endif
