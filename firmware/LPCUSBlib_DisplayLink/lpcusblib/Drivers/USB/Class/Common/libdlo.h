#ifndef DLO_LIBDLO_H
#define DLO_LIBDLO_H        /**< Avoid multiple inclusion. */

#include <stdlib.h>
#include <stdint.h>
#include "dejavusansbold9.h"
#include "../Host/DisplayLinkClassHost.h"

#ifdef __cplusplus
extern "C" {
#endif

/** Return a 32 bpp colour number when given the three RGB components. */
#define DLO_RGB(red,grn,blu) (dlo_col32_t)(((red) & 0xFF) | (((grn) & 0xFF) << 8) | (((blu) & 0xFF) << 16))

/** Set the red component (0..255) of a 32 bpp colour. */
#define DLO_RGB_SETRED(col,red) (dlo_col32_t)(((col) & ~0xFF) | ((red) & 0xFF))

/** Set the green component (0..255) of a 32 bpp colour. */
#define DLO_RGB_SETGRN(col,grn) (dlo_col32_t)(((col) & ~0xFF00) | (((grn) & 0xFF) << 8))

/** Set the blue component (0..255) of a 32 bpp colour. */
#define DLO_RGB_SETBLU(col,blu) (dlo_col32_t)(((col) & ~0xFF0000) | (((blu) & 0xFF) << 16))

/** Read the red component (0..255) of a 32 bpp colour. */
#define DLO_RGB_GETRED(col) (uint8_t)((col) & 0xFF)

/** Read the green component (0..255) of a 32 bpp colour. */
#define DLO_RGB_GETGRN(col) (uint8_t)(((col) >> 8) & 0xFF)

/** Read the blue component (0..255) of a 32 bpp colour. */
#define DLO_RGB_GETBLU(col) (uint8_t)(((col) >> 16) & 0xFF)

extern dlo_mode_t *dlo_get_mode(const dlo_dev_t uid);

extern dlo_retcode_t dlo_fill_rect(USB_ClassInfo_MS_Host_t * const DLIntInfo,
		const dlo_view_t * const view, const dlo_rect_t * const rec,
		const dlo_col32_t col);

extern dlo_retcode_t dlo_draw_str(USB_ClassInfo_MS_Host_t * const DLIntInfo,
		const dlo_dot_t * const posIni, const dlo_col32_t col,
		const FONT_INFO *fontInfo, char *str);

extern dlo_retcode_t dlo_draw_char(USB_ClassInfo_MS_Host_t * const DLIntInfo,
		const dlo_dot_t * const posIni, const dlo_col32_t col,
		const char *glyph, uint8_t cols, uint8_t rows, uint8_t flush);

extern dlo_retcode_t dlo_draw_line(USB_ClassInfo_MS_Host_t * const DLIntInfo,
		dlo_dot_t * posIni, dlo_dot_t * posFin, const dlo_col32_t col,
		uint16_t grosor);

extern dlo_retcode_t dlo_draw_arc(USB_ClassInfo_MS_Host_t * const DLIntInfo,
		dlo_dot_t * posCenter, uint16_t r, drawCorners_t corner,
		const dlo_col32_t col);

extern dlo_retcode_t dlo_draw_circle(USB_ClassInfo_MS_Host_t * const DLIntInfo,
		dlo_dot_t * posCenter, uint16_t r,uint16_t grosor,const dlo_col32_t col);

extern dlo_retcode_t dlo_draw_circle_fill(USB_ClassInfo_MS_Host_t * const DLIntInfo,
		dlo_dot_t * posCenter, uint16_t r,const dlo_col32_t col);

#ifdef __cplusplus
};
#endif

#endif
