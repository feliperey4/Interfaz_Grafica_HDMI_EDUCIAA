/** @file libdlo.c
 *
 *  @brief This file implements the high-level API for the DisplayLink libdlo library.
 *
 *  DisplayLink Open Source Software (libdlo)
 *  Copyright (C) 2009, DisplayLink
 *  www.displaylink.com
 *
 *  This library is free software; you can redistribute it and/or modify it under
 *  the terms of the GNU Library General Public License as published by the Free
 *  Software Foundation; LGPL version 2, dated June 1991.
 *
 *  This library is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this library; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <string.h>
#include "dlo_defs.h"
#include "dlo_grfx.h"
#include "dlo_mode.h"

//#include "../Host/DisplayLinkClassHost.h"

/* File-scope defines ------------------------------------------------------------------*/

/** Structure to hold information about how a rectangle may have been clipped
 */
typedef struct clip_s {
	uint32_t left; /**< Number of pixels clipped from left edge. */
	uint32_t right; /**< Number of pixels clipped from right edge. */
	uint32_t below; /**< Number of pixels clipped from bottom edge. */
	uint32_t above; /**< Number of pixels clipped from top edge. */
} clip_t; /**< A struct @a clip_s. */

/** Return value from function to check source and destination viewports in the rectangle copy function.
 */
typedef enum {
	vstat_good_view = 0, /**< Viewports are both good, not overlapping. */
	vstat_good_overlap, /**< Viewports are both the same. */
	vstat_bad_view, /**< Something is wrong with one or other viewport. */
	vstat_bad_overlap /**< Viewports overlap but are not identical. */
} vstat_t;

/* File-scope inline functions ---------------------------------------------------------*/

/* File-scope types --------------------------------------------------------------------*/

/* External-scope variables ------------------------------------------------------------*/

char err_file[1024] = { '\0' };

uint32_t err_line = 0;

/* File-scope variables ----------------------------------------------------------------*/

/** Pointer to the head node in the device list.
 */
static dlo_device_t *dev_list = NULL;

/** Flag is toggled at each enumeration to spot removed devices.
 */
static uint8_t check_state = false;

/* File-scope function declarations ----------------------------------------------------*/

static uint8_t sanitise_view_rect(const dlo_device_t * const dev,
		const dlo_view_t * const view, const dlo_rect_t * const rec,
		dlo_area_t * const area, clip_t * const clip);

static vstat_t check_overlaps(const dlo_device_t * const dev,
		const dlo_view_t * const src_view, const dlo_view_t * const dest_view);

static dlo_retcode_t dlo_fill_rect_flush(
		USB_ClassInfo_MS_Host_t * const DLIntInfo,
		const dlo_view_t * const view, const dlo_rect_t * const rec,
		const dlo_col32_t col, uint8_t flush);

static dlo_retcode_t dlo_draw_pixel(USB_ClassInfo_MS_Host_t * const DLIntInfo,
		int32_t x, int32_t y, const dlo_col32_t col);

static dlo_retcode_t dlo_draw_circle_fit(
		USB_ClassInfo_MS_Host_t * const DLIntInfo, dlo_dot_t * posCenter,
		uint16_t r, const dlo_col32_t col);

dlo_mode_t *dlo_get_mode(const dlo_dev_t uid) {
	dlo_device_t *dev = (dlo_device_t *) uid;

	return dev ? &(dev->mode) : NULL;
}

dlo_retcode_t dlo_fill_rect(USB_ClassInfo_MS_Host_t * const DLIntInfo,
		const dlo_view_t * const view, const dlo_rect_t * const rec,
		const dlo_col32_t col) {
	return dlo_fill_rect_flush(DLIntInfo, view, rec, col, FORCE_FLUSH);
}

dlo_retcode_t dlo_draw_str(USB_ClassInfo_MS_Host_t * const DLIntInfo,
		const dlo_dot_t * const posIni, const dlo_col32_t col,
		const FONT_INFO *fontInfo, char *str) {

	uint16_t charWidth, characterToOutput;
	const FONT_CHAR_INFO *charInfo;
	uint16_t charOffset;
	dlo_dot_t pos;

	pos.x = posIni->x;
	pos.y = posIni->y;

	// while not NULL
	while (*str != '\0') {
		// get character to output
		characterToOutput = *str;

		// get char info
		charInfo = fontInfo->charInfo;

		// some fonts have character descriptors, some don't
		if (charInfo != NULL) {
			// get correct char offset
			charInfo += (characterToOutput - fontInfo->startChar);

			// get width from char info
			charWidth = charInfo->widthBits;

			// get offset from char info
			charOffset = charInfo->offset;
		} else {
			// if no char info, char width is always 5
			charWidth = 5;

			// char offset - assume 5 * letter offset
			charOffset = (characterToOutput - fontInfo->startChar) * 5;
		}

		// Send individual characters
		// We need to manually calculate width in pages since this is screwy with variable width fonts
		//uint8_t heightPages = charWidth % 8 ? charWidth / 8 : charWidth / 8 + 1;
		dlo_draw_char(DLIntInfo, &pos, col,
				(const char *) (&fontInfo->data[charOffset]), charWidth,
				fontInfo->height, FORCE_FLUSH);

		// next char X
		pos.x += charWidth + 1;

		// next char
		str++;
	}
	return dlo_ok;
}

dlo_retcode_t dlo_draw_char(USB_ClassInfo_MS_Host_t * const DLIntInfo,
		const dlo_dot_t * const posIni, const dlo_col32_t col,
		const char *glyph, uint8_t cols, uint8_t rows, uint8_t flush) {

	uint16_t currentY, currentX, indexIntoGlyph;
	uint16_t _row, _col, _colPages;
	dlo_rect_t rec;
	dlo_view_t *view;
	char aux;

	view = &DLIntInfo->dev->mode.view;
	rec.origin.y = posIni->y;
	rec.origin.x = posIni->x;
	rec.height = 1;
	rec.width = 1;

// Figure out how many columns worth of data we have
	if (cols % 8)
		_colPages = cols / 8 + 1;
	else
		_colPages = cols / 8;

	for (_row = 0; _row < rows; _row++) {
		for (_col = 0; _col < _colPages; _col++) {
			if (_row == 0)
				indexIntoGlyph = _col;
			else
				indexIntoGlyph = (_row * _colPages) + _col;

			rec.origin.y = posIni->y + _row;
			rec.origin.x = posIni->x + (_col * 8);
			// send the data byte
			aux = glyph[indexIntoGlyph];
			if (glyph[indexIntoGlyph] & (0X80)) {
				dlo_fill_rect_flush(DLIntInfo, view, &rec, col, AUTO_FUSH);

			}
			rec.origin.x += rec.width;
			if (glyph[indexIntoGlyph] & (0X40)) {
				dlo_fill_rect_flush(DLIntInfo, view, &rec, col, AUTO_FUSH);
			}
			rec.origin.x += rec.width;
			if (glyph[indexIntoGlyph] & (0X20)) {
				dlo_fill_rect_flush(DLIntInfo, view, &rec, col, AUTO_FUSH);
			}
			rec.origin.x += rec.width;
			if (glyph[indexIntoGlyph] & (0X10)) {
				dlo_fill_rect_flush(DLIntInfo, view, &rec, col, AUTO_FUSH);
			}
			rec.origin.x += rec.width;
			if (glyph[indexIntoGlyph] & (0X08)) {
				dlo_fill_rect_flush(DLIntInfo, view, &rec, col, AUTO_FUSH);
			}
			rec.origin.x += rec.width;
			if (glyph[indexIntoGlyph] & (0X04)) {
				dlo_fill_rect_flush(DLIntInfo, view, &rec, col, AUTO_FUSH);
			}
			rec.origin.x += rec.width;
			if (glyph[indexIntoGlyph] & (0X02)) {
				dlo_fill_rect_flush(DLIntInfo, view, &rec, col, AUTO_FUSH);
			}
			rec.origin.x += rec.width;
			if (glyph[indexIntoGlyph] & (0X01)) {
				dlo_fill_rect_flush(DLIntInfo, view, &rec, col, flush);
			}
			rec.origin.x += rec.width;
		}
	}
	return dlo_ok;
}

dlo_retcode_t dlo_draw_line(USB_ClassInfo_MS_Host_t * const DLIntInfo,
		dlo_dot_t * posIni, dlo_dot_t * posFin, const dlo_col32_t col,
		uint16_t solid) {

	dlo_rect_t rec;
	dlo_view_t *view;

	view = &DLIntInfo->dev->mode.view;
	rec.origin.y = posIni->y;
	rec.origin.x = posIni->x;
	rec.height = 1;
	rec.width = 1;

	if (solid == 0) {
		return dlo_ok;
	}

	// Check if we can use the optimised horizontal line method
	if ((posIni->y == posFin->y)) {
		rec.origin.y = posIni->y;
		rec.origin.x = posIni->x < posFin->x ? posIni->x : posFin->x;
		rec.height = solid;
		rec.width =
				posIni->x < posFin->x ?
						posFin->x - posIni->x : posIni->x - posFin->x;

		dlo_fill_rect_flush(DLIntInfo, view, &rec, col, AUTO_FUSH);
		return dlo_ok;
	}

	if ((posIni->x == posFin->x)) {
		rec.origin.x = posIni->x;
		rec.origin.y = posIni->y < posFin->y ? posIni->y : posFin->y;
		rec.width = solid;
		rec.height =
				posIni->y < posFin->y ?
						posFin->y - posIni->y : posIni->y - posFin->y;

		dlo_fill_rect_flush(DLIntInfo, view, &rec, col, AUTO_FUSH);
		return dlo_ok;
	}

	// Draw non-horizontal or dotted line
	int dy = posFin->y - posIni->y;
	int dx = posFin->x - posIni->x;
	int stepx, stepy;

	if (dy < 0) {
		dy = -dy;
		stepy = -1;
	} else {
		stepy = 1;
	}
	if (dx < 0) {
		dx = -dx;
		stepx = -1;
	} else {
		stepx = 1;
	}
	dy <<= 1;
	dx <<= 1;

	rec.origin.x = posIni->x;
	rec.origin.y = posIni->y;
	rec.width = solid;
	rec.height = solid;
	dlo_fill_rect_flush(DLIntInfo, view, &rec, col, AUTO_FUSH);

	if (dx > dy) {
		int fraction = dy - (dx >> 1);
		while (posIni->x != posFin->x) {
			if (fraction >= 0) {
				posIni->y += stepy;
				fraction -= dx;
			}
			posIni->x += stepx;
			fraction += dy;

			rec.origin.x = posIni->x;
			rec.origin.y = posIni->y;
			rec.width = 1;
			rec.height = solid;
			dlo_fill_rect_flush(DLIntInfo, view, &rec, col, AUTO_FUSH);

		}
	} else {
		int fraction = dx - (dy >> 1);
		while (posIni->y != posFin->y) {
			if (fraction >= 0) {
				posIni->x += stepx;
				fraction -= dy;
			}
			posIni->y += stepy;
			fraction += dx;

			rec.origin.x = posIni->x;
			rec.origin.y = posIni->y;
			rec.width = solid;
			rec.height = 1;
			dlo_fill_rect_flush(DLIntInfo, view, &rec, col, AUTO_FUSH);
		}
	}

	return dlo_ok;
}

dlo_retcode_t dlo_draw_arc(USB_ClassInfo_MS_Host_t * const DLIntInfo,
		dlo_dot_t * posCenter, uint16_t r, drawCorners_t corner,
		const dlo_col32_t col) {

	int16_t f = 1 - r;
	int16_t ddF_x = 1;
	int16_t ddF_y = -2 * r;
	int16_t x = 0;
	int16_t y = r;

	while (x < y) {
		if (f >= 0) {
			y--;
			ddF_y += 2;
			f += ddF_y;
		}
		x++;
		ddF_x += 2;
		f += ddF_x;
		if (corner & DRAW_CORNERS_BOTTOMRIGHT) {
			dlo_draw_pixel(DLIntInfo, posCenter->x + x, posCenter->y + y, col);
			dlo_draw_pixel(DLIntInfo, posCenter->x + y, posCenter->y + x, col);
		}
		if (corner & DRAW_CORNERS_TOPRIGHT) {
			dlo_draw_pixel(DLIntInfo, posCenter->x + x, posCenter->y - y, col);
			dlo_draw_pixel(DLIntInfo, posCenter->x + y, posCenter->y - x, col);
		}
		if (corner & DRAW_CORNERS_BOTTOMLEFT) {
			dlo_draw_pixel(DLIntInfo, posCenter->x - y, posCenter->y + x, col);
			dlo_draw_pixel(DLIntInfo, posCenter->x - x, posCenter->y + y, col);
		}
		if (corner & DRAW_CORNERS_TOPLEFT) {
			dlo_draw_pixel(DLIntInfo, posCenter->x - y, posCenter->y - x, col);
			dlo_draw_pixel(DLIntInfo, posCenter->x - x, posCenter->y - y, col);
		}
	}

	return dlo_ok;
}

dlo_retcode_t dlo_draw_circle(USB_ClassInfo_MS_Host_t * const DLIntInfo,
		dlo_dot_t * posCenter, uint16_t r, uint16_t grosor,
		const dlo_col32_t col) {
	uint32_t i;
	for (i = 0; i < grosor; i++) {
		if (r - i == 0)
			break;
		dlo_draw_circle_fit(DLIntInfo, posCenter, r - i, col);
	}
	return dlo_ok;
}

dlo_retcode_t dlo_draw_circle_fill(USB_ClassInfo_MS_Host_t * const DLIntInfo,
		dlo_dot_t * posCenter, uint16_t r, const dlo_col32_t col) {

	float co = 0.70710678118 * r;
	dlo_rect_t rec;
	dlo_view_t *view;

	view = &DLIntInfo->dev->mode.view;
	rec.origin.y = posCenter->y - co;
	rec.origin.x = posCenter->x - co;
	rec.height = 2*co;
	rec.width = 2*co;
	dlo_fill_rect_flush(DLIntInfo, view, &rec, col, AUTO_FUSH);

	dlo_draw_circle(DLIntInfo, posCenter, r, r/3, col);
	return dlo_ok;
}

//dlo_retcode_t dlo_draw_circle_fill(USB_ClassInfo_MS_Host_t * const DLIntInfo,
//		dlo_dot_t * posCenter, uint16_t radius, const dlo_col32_t col) {
//	int16_t f = 1 - radius;
//	int16_t ddF_x = 1;
//	int16_t ddF_y = -2 * radius;
//	int16_t x = 0;
//	int16_t y = radius;
//	int16_t xc_px, yc_my, xc_mx, xc_py, yc_mx, xc_my;
//	int16_t lcdWidth;
//	dlo_dot_t posIni;
//	dlo_dot_t posFin;
//	dlo_view_t *view;
//
//	view = &DLIntInfo->dev->mode.view;
//	lcdWidth = view->width;
//
//	if (posCenter->x < lcdWidth) {
//		posIni.x = posCenter->x;
//		posIni.y = posCenter->y - radius < 0 ? 0 : posCenter->y - radius;
//		posFin.x = posCenter->x;
//		posFin.y = (posCenter->y - radius) + (2 * radius);
//		dlo_draw_line(DLIntInfo, &posIni, &posFin, col, 1);
//	}
//
//	while (x < y) {
//		if (f >= 0) {
//			y--;
//			ddF_y += 2;
//			f += ddF_y;
//		}
//		x++;
//		ddF_x += 2;
//		f += ddF_x;
//
//		xc_px = posCenter->x + x;
//		xc_mx = posCenter->x - x;
//		xc_py = posCenter->x + y;
//		xc_my = posCenter->x - y;
//		yc_mx = posCenter->y - x;
//		yc_my = posCenter->y - y;
//
//		// Make sure X positions are not negative or too large or the pixels will
//		// overflow.  Y overflow is handled in drawLine().
//		if ((xc_px < lcdWidth) && (xc_px >= 0)) {
//			posIni.x = xc_px;
//			posIni.y = yc_my;
//			posFin.x = xc_px;
//			posFin.y = yc_my + 2 * y;
//			dlo_draw_line(DLIntInfo, &posIni, &posFin, col, 1);
//		}
//
//		if ((xc_mx < lcdWidth) && (xc_mx >= 0)) {
//			posIni.x = xc_mx;
//			posIni.y = yc_my;
//			posFin.x = xc_mx;
//			posFin.y = yc_my + 2 * y;
//			dlo_draw_line(DLIntInfo, &posIni, &posFin, col, 1);
//		}
//
//		if ((xc_py < lcdWidth) && (xc_py >= 0)) {
//			posIni.x = xc_py;
//			posIni.y = yc_mx;
//			posFin.x = xc_py;
//			posFin.y = yc_mx + 2 * x;
//			dlo_draw_line(DLIntInfo, &posIni, &posFin, col, 1);
//		}
//
//		if ((xc_my < lcdWidth) && (xc_my >= 0)) {
//			posIni.x = xc_my;
//			posIni.y = yc_mx;
//			posFin.x = xc_my;
//			posFin.y = yc_mx + 2 * x;
//			dlo_draw_line(DLIntInfo, &posIni, &posFin, col, 1);
//		}
//	}
//	return dlo_ok;
//}
/* File-scope function definitions -----------------------------------------------------*/

static dlo_retcode_t dlo_draw_circle_fit(
		USB_ClassInfo_MS_Host_t * const DLIntInfo, dlo_dot_t * posCenter,
		uint16_t r, const dlo_col32_t col) {

	dlo_draw_pixel(DLIntInfo, posCenter->x, posCenter->y + r, col);
	dlo_draw_pixel(DLIntInfo, posCenter->x, posCenter->y - r, col);
	dlo_draw_pixel(DLIntInfo, posCenter->x + r, posCenter->y, col);
	dlo_draw_pixel(DLIntInfo, posCenter->x - r, posCenter->y, col);
	dlo_draw_arc(DLIntInfo, posCenter, r, DRAW_CORNERS_ALL, col);
	return dlo_ok;
}
static dlo_retcode_t dlo_draw_pixel(USB_ClassInfo_MS_Host_t * const DLIntInfo,
		int32_t x, int32_t y, const dlo_col32_t col) {
	dlo_rect_t rec;
	dlo_view_t *view;

	view = &DLIntInfo->dev->mode.view;
	rec.origin.y = y;
	rec.origin.x = x;
	rec.height = 1;
	rec.width = 1;
	return dlo_fill_rect_flush(DLIntInfo, view, &rec, col, AUTO_FUSH);
}

static dlo_retcode_t dlo_fill_rect_flush(
		USB_ClassInfo_MS_Host_t * const DLIntInfo,
		const dlo_view_t * const view, const dlo_rect_t * const rec,
		const dlo_col32_t col, uint8_t flush) {
	static clip_t clip;
	static dlo_area_t area;
	dlo_device_t * const dev = DLIntInfo->dev;

	if (!dev)
		return dlo_err_bad_device;

	/* Clip the rectangle to its viewport edges */
	if (!sanitise_view_rect(dev, view, rec, &area, &clip))
		return dlo_ok;

	return dlo_grfx_fill_rect(DLIntInfo, &area, col, flush);
}

static uint8_t sanitise_view_rect(const dlo_device_t * const dev,
		const dlo_view_t * const view, const dlo_rect_t * const rec,
		dlo_area_t * const area, clip_t * const clip) {
	static dlo_rect_t my_rec;
	const dlo_view_t * const my_view = view ? view : &(dev->mode.view);
	uint32_t pix_off;

	/* Exit here if my_view is still NULL (i.e. no screen mode is set up yet) */
	if (!my_view)
		return false;

	/* Default the rectangle to the whole of the the view, if none was specified */
	if (rec)
		my_rec = *rec;
	else {
		my_rec.origin.x = 0;
		my_rec.origin.y = 0;
		my_rec.width = my_view->width;
		my_rec.height = my_view->height;
	}

	/* Exit here if the source rectangle is never going to appear in the viewport */
	if (my_rec.origin.x + (int32_t) my_rec.width < 0
			|| my_rec.origin.y + (int32_t) my_rec.height < 0)
		return false;
	if (my_rec.origin.x >= (int32_t) my_view->width
			|| my_rec.origin.y >= (int32_t) my_view->height)
		return false;

	/* Initialise the clip structure to empty */
	clip->left = 0;
	clip->right = 0;
	clip->below = 0;
	clip->above = 0;

	/* Now clip the rectangle to lie within the viewport */
	if (my_rec.origin.x + (int32_t) my_rec.width > (int32_t) my_view->width) {
		clip->right = my_rec.width - (my_view->width - my_rec.origin.x);
		my_rec.width = my_view->width - my_rec.origin.x;
	}
	if (my_rec.origin.y + (int32_t) my_rec.height > (int32_t) my_view->height) {
		clip->above = my_rec.height - (my_view->height - my_rec.origin.y);
		my_rec.height = my_view->height - my_rec.origin.y;
	}
	if (my_rec.origin.x < 0) {
		clip->left = -my_rec.origin.x;
		my_rec.width += my_rec.origin.x;
		my_rec.origin.x = 0;
	}
	if (my_rec.origin.y < 0) {
		clip->below = -my_rec.origin.y;
		my_rec.height += my_rec.origin.y;
		my_rec.origin.y = 0;
	}

	/* Exit here if we've clipped the rectangle away to nothing */
	if (!my_rec.width || !my_rec.height)
		return false;

	/* Create the clipped area structure */
	pix_off = my_rec.origin.x + (my_rec.origin.y * my_view->width);
	area->view.width = my_rec.width;
	area->view.height = my_rec.height;
	area->view.bpp = my_view->bpp;
	area->view.base = my_view->base + (BYTES_PER_16BPP * pix_off);
	area->base8 = my_view->base
			+ (BYTES_PER_16BPP * my_view->width * my_view->height) + pix_off;
	area->stride = my_view->width;

	return true;
}

static vstat_t check_overlaps(const dlo_device_t * const dev,
		const dlo_view_t * const src_view, const dlo_view_t * const dest_view) {
	const dlo_view_t * const src = src_view ? src_view : &(dev->mode.view);
	const dlo_view_t * const dest = dest_view ? dest_view : &(dev->mode.view);
	dlo_ptr_t src_end;
	dlo_ptr_t dest_end;

	/* If no display mode is set up, we should stop here to avoid null dereferences */
	if (!src || !dest)
		return vstat_bad_view;

	/* If base addresses are the same, the other attributes must also be the same */
	if (src->base == dest->base) {
		return (src->width == dest->width && src->height == dest->height
				&& src->bpp == dest->bpp) ?
				vstat_good_overlap : vstat_bad_overlap;
	}

	/* Ensure we're happy with the source bits per pixel */
	if (src->bpp != 16 && src->bpp != 24)
		return vstat_bad_view;

	/* Ensure we're happy with the destination bits per pixel */
	if (dest->bpp != 16 && dest->bpp != 24)
		return vstat_bad_view;

	/* Compute the end addresses of both views */
	src_end = src->base + (BYTES_PER_16BPP * src->width * src->height);
	dest_end = dest->base + (BYTES_PER_16BPP * dest->width * dest->height);
	if (src->bpp == 24)
		src_end += BYTES_PER_8BPP * src->width * src->height;
	if (dest->bpp == 24)
		dest_end += BYTES_PER_8BPP * dest->width * dest->height;

	/* Any overlap in address range means we have a problem */
	return (dest_end <= src->base || dest->base >= src_end) ?
			vstat_good_view : vstat_bad_overlap;
}

/* End of file -------------------------------------------------------------------------*/
