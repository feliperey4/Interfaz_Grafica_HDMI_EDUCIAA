
#ifndef _MS_CLASS_H_
#define _MS_CLASS_H_

	/* Macros: */
		#define __INCLUDE_FROM_USB_DRIVER
		#define __INCLUDE_FROM_DL_DRIVER

	/* Includes: */
		#include "../Core/USBMode.h"

		#if defined(USB_CAN_BE_DEVICE)
			#include "Device/MassStorageClassDevice.h"
		#endif

		#if defined(USB_CAN_BE_HOST)
			#include "Host/DisplayLinkClassHost.h"
		#endif

#endif

/** @} */

